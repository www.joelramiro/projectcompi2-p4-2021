.data
string0: .asciiz "a = "
string1: .asciiz "b = "
string2: .asciiz "a+b = "
string3: .asciiz "a-b = "
string4: .asciiz "a*b = "
string5: .asciiz "a/b = "
string6: .asciiz "a%b = "
string9: .asciiz "a>b = "
string12: .asciiz "a<b = "
string13: .asciiz "a>=b = "
string16: .asciiz "a<=b = "
string17: .asciiz "true&&true = "

.globl main
.text
main: 
addiu $sp, $sp, -12


sw $ra, 0($sp)

li $t0, 12

sw $t0, 4($sp)

li $t0, 4

sw $t0, 8($sp)


la $a0, string0
li $v0, 4
syscall
lw $t0, 4($sp)

move $a0, $t0
li $v0, 1
syscall
li $a0, 10
li $v0, 11
syscall


la $a0, string1
li $v0, 4
syscall
lw $t0, 8($sp)

move $a0, $t0
li $v0, 1
syscall
li $a0, 10
li $v0, 11
syscall


la $a0, string2
li $v0, 4
syscall
lw $t0, 4($sp)

lw $t1, 8($sp)

add $t2, $t0, $t1

move $a0, $t2
li $v0, 1
syscall
li $a0, 10
li $v0, 11
syscall


la $a0, string3
li $v0, 4
syscall
lw $t0, 4($sp)

lw $t1, 8($sp)

sub $t2, $t0, $t1

move $a0, $t2
li $v0, 1
syscall
li $a0, 10
li $v0, 11
syscall


la $a0, string4
li $v0, 4
syscall
lw $t0, 4($sp)

lw $t1, 8($sp)

mult $t0, $t1
mflo $t2

move $a0, $t2
li $v0, 1
syscall
li $a0, 10
li $v0, 11
syscall


la $a0, string5
li $v0, 4
syscall
lw $t0, 4($sp)

lw $t1, 8($sp)

div $t0, $t1
mflo $t2

move $a0, $t2
li $v0, 1
syscall
li $a0, 10
li $v0, 11
syscall


la $a0, string6
li $v0, 4
syscall
lw $t0, 4($sp)

lw $t1, 8($sp)

move $t3, $t0
modStart7: 
bgt $t1, $t3, modEnd8
sub $t3, $t3, $t1
j modStart7
modEnd8:
move $t2, $t3


move $a0, $t2
li $v0, 1
syscall
li $a0, 10
li $v0, 11
syscall


la $a0, string9
li $v0, 4
syscall
lw $t0, 4($sp)

lw $t1, 8($sp)

bgt $t0, $t1, label10
addi $t2, $zero, 0
 j finalLabel11
label10:
addi $t2, $zero, 1
finalLabel11:

move $a0, $t2
li $v0, 1
syscall
li $a0, 10
li $v0, 11
syscall


la $a0, string12
li $v0, 4
syscall
lw $t0, 4($sp)

lw $t1, 8($sp)

slt $t0, $t0, $t1

move $a0, $t0
li $v0, 1
syscall
li $a0, 10
li $v0, 11
syscall


la $a0, string13
li $v0, 4
syscall
lw $t0, 4($sp)

lw $t1, 8($sp)

bge $t0, $t1, label14
addi $t2, $zero, 0
 j finalLabel15
label14:
addi $t2, $zero, 1
finalLabel15:

move $a0, $t2
li $v0, 1
syscall
li $a0, 10
li $v0, 11
syscall


la $a0, string16
li $v0, 4
syscall
lw $t0, 4($sp)

lw $t1, 8($sp)

sle $t0, $t0, $t1

move $a0, $t0
li $v0, 1
syscall
li $a0, 10
li $v0, 11
syscall


la $a0, string17
li $v0, 4
syscall
li $t0, 1

li $t1, 0

addi $t2, $zero, 0
beq $t0, $t2, finalLabel19
beq $t1, $t2, finalLabel19
label18:
addi $t2, $zero, 1
finalLabel19:

move $a0, $t2
li $v0, 1
syscall
li $a0, 10
li $v0, 11
syscall


lw $ra, 0($sp)
addiu $sp, $sp, 12
jr $ra

