enum Type{
    INVALID,
    STRING,
    INT,
    FLOAT,
    INT_ARRAY,
    FLOAT_ARRAY,
    BOOL,
    VOID
};