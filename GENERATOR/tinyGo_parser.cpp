/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.5.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 5 "tinyGo.y"

    #include <cstdio>
    #include "asm.h"
    #include <fstream>
    #include <iostream>

    using namespace std;
    int yylex();
    extern int yylineno;
    void yyerror(const char * s){
        fprintf(stderr, "Line: %d, error: %s\n", yylineno, s);
    }

    #define YYERROR_VERBOSE 1
    #define YYDEBUG 1
    #define EQUAL 1
    #define PLUSEQUAL 2
    #define MINUSEQUAL 3
    #define POTOREQUAL 4
    #define DIVOREQUAL 5
    #define MULTOREQUAL 6
    #define PORCENTOREQUAL 7
    #define COLONOREQUAL 8
    #define OREQUAL 9
    #define ANDEQUAL 10

    Asm assemblyFile;
    void writeFile(string name){
        ofstream file;
        file.open(name);
        file << assemblyFile.data << endl
        << assemblyFile.global <<endl
        << assemblyFile.text << endl;
        file.close();
    }


#line 108 "tinyGo_parser.cpp"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_TOKENS_H_INCLUDED
# define YY_YY_TOKENS_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 1 "tinyGo.y"

     #include "ast.h"

#line 155 "tinyGo_parser.cpp"

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    TK_LIT_STRING = 258,
    TK_ID = 259,
    TK_LIT_INT = 260,
    TK_LIT_FLOAT = 261,
    TK_FALSE = 262,
    TK_TRUE = 263,
    EOL = 264,
    TK_PRINTF = 265,
    TK_ADD = 266,
    TK_SUB = 267,
    TK_LESS = 268,
    TK_LEFT_BRACKET = 269,
    TK_RIGHT_BRACKET = 270,
    TK_MUL = 271,
    TK_POW = 272,
    TK_GREATER = 273,
    TK_DIV = 274,
    TK_EQUAL_EQUAL = 275,
    TK_COMMA = 276,
    TK_SEMICOLON = 277,
    TK_PERCENT = 278,
    TK_EXCLAMATION = 279,
    TK_COLON = 280,
    TK_LEFT_KEY = 281,
    TK_RIGHT_KEY = 282,
    TK_LEFT_PARENTHESES = 283,
    TK_RIGHT_PARENTHESES = 284,
    TK_INT = 285,
    TK_FLOAT = 286,
    TK_BOOL = 287,
    TK_STRING = 288,
    TK_MINUS_MINUS = 289,
    TK_PORCENT_OR_EQUAL = 290,
    TK_COLON_OR_EQUAL = 291,
    TK_PLUS_PLUS = 292,
    TK_DIV_OR_EQUAL = 293,
    TK_GREATER_OR_EQUAL = 294,
    TK_POT_OR_EQUAL = 295,
    TK_MUL_OR_EQUAL = 296,
    TK_LESS_OR_EQUAL = 297,
    TK_OR = 298,
    TK_OR_EQUAL = 299,
    TK_MINUS_EQUAL = 300,
    TK_NOT_EQUAL = 301,
    TK_EQUAL = 302,
    TK_AND = 303,
    TK_AND_EQUAL = 304,
    TK_PLUS_EQUAL = 305,
    TK_IF = 306,
    TK_ELSE = 307,
    TK_FOR = 308,
    TK_BREAK = 309,
    TK_CONTINUE = 310,
    TK_FUNC = 311,
    TK_RETURN = 312,
    TK_MAIN = 313,
    TK_IMPORT = 314,
    TK_PACKAGE = 315,
    TK_VAR = 316
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 43 "tinyGo.y"

    const char * string_t;
    int int_t;
    float float_t;
    Expr * expr_t;
    ArgumentList * argument_list_t;
    ExpressionList* expression_list_t;
    Statement * statement_t;
    StatementList * statement_list_t;
    Parameter * parameter_t;
    ImportList* import_list_t;
    ParameterList * parameter_list_t;
    InitializerElementList * initializer_list_t;
    Initializer * initializer_t;
    InitDeclarator * init_declarator_t;
    Declarator * declarator_t;
    Declaration * declaration_t;
    DeclarationList * declaration_list_t;
    Import* import_t;
    StringValue* string_value_t;

#line 250 "tinyGo_parser.cpp"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_TOKENS_H_INCLUDED  */



#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))

/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                            \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  5
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   327

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  62
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  44
/* YYNRULES -- Number of rules.  */
#define YYNRULES  123
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  223

#define YYUNDEFTOK  2
#define YYMAXUTOK   316


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_int16 yyrline[] =
{
       0,   104,   104,   121,   140,   143,   144,   146,   149,   154,
     161,   164,   165,   168,   169,   173,   177,   181,   186,   191,
     195,   200,   207,   208,   211,   214,   215,   218,   219,   222,
     227,   230,   231,   234,   237,   238,   241,   242,   243,   244,
     245,   246,   249,   250,   254,   255,   259,   264,   269,   270,
     271,   281,   282,   283,   284,   285,   286,   289,   290,   291,
     294,   297,   300,   301,   302,   303,   306,   307,   308,   309,
     312,   313,   314,   317,   318,   321,   322,   323,   324,   325,
     326,   330,   331,   334,   335,   338,   339,   342,   343,   344,
     347,   348,   349,   352,   353,   354,   355,   356,   359,   360,
     361,   364,   365,   368,   369,   373,   374,   375,   376,   377,
     378,   379,   380,   381,   382,   385,   388,   389,   390,   391,
     394,   395,   398,   399
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "TK_LIT_STRING", "TK_ID", "TK_LIT_INT",
  "TK_LIT_FLOAT", "TK_FALSE", "TK_TRUE", "EOL", "TK_PRINTF", "TK_ADD",
  "TK_SUB", "TK_LESS", "TK_LEFT_BRACKET", "TK_RIGHT_BRACKET", "TK_MUL",
  "TK_POW", "TK_GREATER", "TK_DIV", "TK_EQUAL_EQUAL", "TK_COMMA",
  "TK_SEMICOLON", "TK_PERCENT", "TK_EXCLAMATION", "TK_COLON",
  "TK_LEFT_KEY", "TK_RIGHT_KEY", "TK_LEFT_PARENTHESES",
  "TK_RIGHT_PARENTHESES", "TK_INT", "TK_FLOAT", "TK_BOOL", "TK_STRING",
  "TK_MINUS_MINUS", "TK_PORCENT_OR_EQUAL", "TK_COLON_OR_EQUAL",
  "TK_PLUS_PLUS", "TK_DIV_OR_EQUAL", "TK_GREATER_OR_EQUAL",
  "TK_POT_OR_EQUAL", "TK_MUL_OR_EQUAL", "TK_LESS_OR_EQUAL", "TK_OR",
  "TK_OR_EQUAL", "TK_MINUS_EQUAL", "TK_NOT_EQUAL", "TK_EQUAL", "TK_AND",
  "TK_AND_EQUAL", "TK_PLUS_EQUAL", "TK_IF", "TK_ELSE", "TK_FOR",
  "TK_BREAK", "TK_CONTINUE", "TK_FUNC", "TK_RETURN", "TK_MAIN",
  "TK_IMPORT", "TK_PACKAGE", "TK_VAR", "$accept", "start", "package",
  "imports", "importsLitString", "import", "input", "external_declaration",
  "method_definition", "declaration_list", "declaration",
  "init_declarator", "declarator", "initializer", "parameters_type_list",
  "parameter_declaration", "initializer_list", "statement",
  "expression_list", "statement_list", "block_statement", "if_statement",
  "for_statement", "for_expression_statement", "expression_statement",
  "jump_statement", "type", "primary_expression", "assignment_expression",
  "postfix_expression", "argument_expression_list", "percent_expression",
  "pow_expression", "multiplicative_expression", "additive_expression",
  "relational_expression", "equality_expression", "logical_or_expression",
  "logical_and_expression", "assignment_operator", "expression",
  "constant", "boolean", "eol", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_int16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316
};
# endif

#define YYPACT_NINF (-124)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-1)

#define yytable_value_is_error(Yyn) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     -46,    13,    42,    65,  -124,  -124,    65,   -16,  -124,     0,
       9,    56,    59,    65,    91,  -124,    65,  -124,    57,    69,
    -124,    65,    74,    65,   125,   106,    65,    91,  -124,  -124,
    -124,    18,    92,   141,   148,  -124,  -124,  -124,  -124,  -124,
     112,  -124,    99,   125,     2,  -124,   151,    65,   149,   168,
      96,   206,    65,  -124,   151,  -124,    56,   218,  -124,   141,
      65,  -124,  -124,  -124,  -124,  -124,  -124,  -124,   285,   285,
    -124,  -124,  -124,   126,   190,   214,    17,   100,    26,    15,
     191,   185,  -124,  -124,   220,    22,  -124,  -124,   231,  -124,
     151,  -124,  -124,    87,    50,   191,  -124,   212,   285,    88,
    -124,  -124,  -124,  -124,  -124,  -124,  -124,  -124,  -124,  -124,
    -124,  -124,   285,   285,   285,   285,   285,   285,   285,   285,
     285,   285,   285,   285,   285,   285,   285,   125,   215,  -124,
     285,   219,    65,    65,   278,   202,  -124,  -124,   211,    65,
      65,    65,  -124,  -124,    65,   225,  -124,   285,  -124,  -124,
     227,  -124,  -124,    51,   191,    50,   190,   214,   214,    17,
      17,   100,   100,   100,   100,    26,    26,   185,    15,   151,
     285,   285,    97,  -124,   285,    97,  -124,  -124,    65,  -124,
    -124,  -124,   270,  -124,    -8,  -124,  -124,  -124,  -124,  -124,
     125,   191,  -124,   285,  -124,  -124,    60,  -124,   151,  -124,
     194,   285,   230,  -124,  -124,  -124,  -124,   151,  -124,   285,
      65,   208,    12,   151,  -124,  -124,  -124,    12,  -124,  -124,
    -124,  -124,  -124
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       0,     0,     0,     0,     4,     1,   123,     0,   122,     0,
       0,     0,     0,     0,     3,    12,     0,    14,     0,     0,
      10,     0,    27,     0,     0,     0,     0,     2,     6,    11,
      13,     0,     0,     0,     0,    24,    66,    67,    68,    69,
      25,     5,     0,     0,     0,    32,     0,     0,     0,     0,
       0,     0,     0,    20,     0,    33,     0,     0,    21,     9,
       0,    28,   118,    70,   116,   117,   121,   120,     0,     0,
      26,    75,    29,    84,    86,    89,    92,    97,   100,   104,
      74,   102,    71,   119,     0,     0,    17,    31,     0,    19,
       0,     8,     7,     0,    84,    35,   115,     0,     0,     0,
      80,   112,   105,    79,   110,   109,   111,   113,   108,   106,
     114,   107,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    50,
       0,     0,     0,     0,     0,     0,    23,    45,     0,     0,
       0,     0,    36,    40,     0,     0,    15,     0,    30,    72,
       0,    77,    82,     0,    73,    83,    85,    87,    88,    90,
      91,    94,    93,    95,    96,    98,    99,   101,   103,     0,
       0,     0,     0,    59,     0,     0,    64,    63,     0,    62,
      47,    22,     0,    46,     0,    44,    39,    37,    38,    61,
       0,    34,    76,     0,    78,    18,     0,    43,     0,    60,
      51,     0,     0,    58,    65,    48,    49,     0,    81,     0,
       0,    54,     0,     0,    16,    42,    41,     0,    52,    53,
      57,    55,    56
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -124,  -124,  -124,  -124,   195,   246,   249,    55,  -124,   129,
     -79,  -124,   258,  -124,  -124,   223,  -124,  -120,  -124,   135,
     -33,   -69,  -124,  -123,  -124,  -124,   -41,  -124,   -47,    73,
    -124,   157,    35,    64,    81,    45,   169,   -57,   174,  -124,
     -64,  -124,  -124,    -6
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     2,     3,    12,    48,    13,    14,    15,    16,   135,
      17,    23,    43,    70,    44,    45,    93,   137,   196,   138,
     139,   140,   141,   171,   142,   143,    40,    71,    96,    73,
     153,    74,    75,    76,    77,    78,    79,    80,    81,   112,
     144,    82,    83,     7
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
       8,    54,    55,    72,    18,    97,   136,    28,   174,    53,
      30,    95,    20,    58,     1,    33,    90,    35,   185,   206,
      41,    86,    22,    56,    89,    62,    63,    64,    65,    66,
      67,    57,   128,   115,   150,   123,   116,    21,    52,   119,
       9,    59,     5,    10,   120,    11,    85,    42,    52,   129,
      69,   201,   152,    11,    92,   154,   181,   146,    19,   136,
      22,   124,   185,   130,    98,   121,   172,   175,   122,    29,
     178,     4,   193,   130,     6,   131,   132,   133,    99,   134,
     194,   209,    29,    11,   100,    31,   169,   103,    34,   210,
     191,    62,    63,    64,    65,    66,    67,    32,   173,    62,
      63,    64,    65,    66,    67,   181,   197,   198,   147,    20,
     202,   117,   118,    51,   148,     9,    69,   151,    25,   199,
      11,    46,    68,    52,    69,    52,   176,   177,   179,    36,
      37,    38,    39,   186,   187,   188,   195,   213,   189,   200,
      98,    94,   203,   219,    47,   215,   208,     9,   222,   207,
     157,   158,    11,    49,    99,    36,    37,    38,    39,    50,
     100,   101,   102,   103,   104,   211,   105,   106,   165,   166,
     107,   108,   204,   109,   214,   110,   111,    52,    60,   218,
     220,   159,   160,    61,   221,    94,   155,    94,    94,    94,
      94,    94,    94,    94,    94,    94,    94,    94,    94,    94,
     161,   162,   163,   164,   216,    62,    63,    64,    65,    66,
      67,    84,   128,   113,    62,    63,    64,    65,    66,    67,
      94,   128,    62,    63,    64,    65,    66,    67,    52,   180,
      69,   114,    88,   126,   125,   127,   145,    52,   183,    69,
     190,   149,   192,   170,    52,    52,   212,    69,    36,    37,
      38,    39,   199,   130,    91,   131,   132,   133,    26,   134,
     217,    27,   130,    11,   131,   132,   133,   184,   134,    24,
     182,   156,    11,    62,    63,    64,    65,    66,    67,    87,
     128,    62,    63,    64,    65,    66,    67,     6,    62,    63,
      64,    65,    66,    67,     0,   168,    52,   205,    69,   167,
       0,     0,     0,     0,     0,     0,    69,     0,     0,     0,
       0,     0,     0,    69,     0,     0,     0,     0,     0,     0,
       0,   130,     0,   131,   132,   133,     0,   134
};

static const yytype_int16 yycheck[] =
{
       6,    42,    43,    50,     4,    69,    85,    13,   131,    42,
      16,    68,     3,    46,    60,    21,    57,    23,   138,    27,
      26,    54,     4,    21,    57,     3,     4,     5,     6,     7,
       8,    29,    10,    16,    98,    20,    19,    28,    26,    13,
      56,    47,     0,    59,    18,    61,    52,    29,    26,    27,
      28,   174,    99,    61,    60,   112,   135,    90,    58,   138,
       4,    46,   182,    51,    14,    39,   130,   131,    42,    14,
     134,    58,    21,    51,     9,    53,    54,    55,    28,    57,
      29,    21,    27,    61,    34,    28,   127,    37,    14,    29,
     147,     3,     4,     5,     6,     7,     8,    28,   131,     3,
       4,     5,     6,     7,     8,   184,   170,   171,    21,     3,
     174,    11,    12,    14,    27,    56,    28,    29,    59,    22,
      61,    29,    26,    26,    28,    26,   132,   133,   134,    30,
      31,    32,    33,   139,   140,   141,   169,   201,   144,   172,
      14,    68,   175,   212,     3,   209,   193,    56,   217,   190,
     115,   116,    61,     5,    28,    30,    31,    32,    33,    47,
      34,    35,    36,    37,    38,   198,    40,    41,   123,   124,
      44,    45,   178,    47,   207,    49,    50,    26,    29,   212,
     213,   117,   118,    15,   217,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     119,   120,   121,   122,   210,     3,     4,     5,     6,     7,
       8,     5,    10,    23,     3,     4,     5,     6,     7,     8,
     147,    10,     3,     4,     5,     6,     7,     8,    26,    27,
      28,    17,    14,    48,    43,    15,     5,    26,    27,    28,
      15,    29,    15,    28,    26,    26,    52,    28,    30,    31,
      32,    33,    22,    51,    59,    53,    54,    55,    12,    57,
      52,    12,    51,    61,    53,    54,    55,   138,    57,    11,
     135,   114,    61,     3,     4,     5,     6,     7,     8,    56,
      10,     3,     4,     5,     6,     7,     8,     9,     3,     4,
       5,     6,     7,     8,    -1,   126,    26,    27,    28,   125,
      -1,    -1,    -1,    -1,    -1,    -1,    28,    -1,    -1,    -1,
      -1,    -1,    -1,    28,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    51,    -1,    53,    54,    55,    -1,    57
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,    60,    63,    64,    58,     0,     9,   105,   105,    56,
      59,    61,    65,    67,    68,    69,    70,    72,     4,    58,
       3,    28,     4,    73,    74,    59,    67,    68,   105,    69,
     105,    28,    28,   105,    14,   105,    30,    31,    32,    33,
      88,   105,    29,    74,    76,    77,    29,     3,    66,     5,
      47,    14,    26,    82,    88,    88,    21,    29,    82,   105,
      29,    15,     3,     4,     5,     6,     7,     8,    26,    28,
      75,    89,    90,    91,    93,    94,    95,    96,    97,    98,
      99,   100,   103,   104,     5,   105,    82,    77,    14,    82,
      88,    66,   105,    78,    91,    99,    90,   102,    14,    28,
      34,    35,    36,    37,    38,    40,    41,    44,    45,    47,
      49,    50,   101,    23,    17,    16,    19,    11,    12,    13,
      18,    39,    42,    20,    46,    43,    48,    15,    10,    27,
      51,    53,    54,    55,    57,    71,    72,    79,    81,    82,
      83,    84,    86,    87,   102,     5,    82,    21,    27,    29,
     102,    29,    90,    92,    99,    91,    93,    94,    94,    95,
      95,    96,    96,    96,    96,    97,    97,   100,    98,    88,
      28,    85,   102,    82,    85,   102,   105,   105,   102,   105,
      27,    72,    81,    27,    71,    79,   105,   105,   105,   105,
      15,    99,    15,    21,    29,    82,    80,   102,   102,    22,
      82,    85,   102,    82,   105,    27,    27,    88,    90,    21,
      29,    82,    52,   102,    82,   102,   105,    52,    82,    83,
      82,    82,    83
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_int8 yyr1[] =
{
       0,    62,    63,    63,    64,    65,    65,    65,    66,    66,
      67,    68,    68,    69,    69,    70,    70,    70,    70,    70,
      70,    70,    71,    71,    72,    73,    73,    74,    74,    75,
      75,    76,    76,    77,    78,    78,    79,    79,    79,    79,
      79,    79,    80,    80,    81,    81,    82,    82,    82,    82,
      82,    83,    83,    83,    83,    83,    83,    84,    84,    84,
      85,    86,    87,    87,    87,    87,    88,    88,    88,    88,
      89,    89,    89,    90,    90,    91,    91,    91,    91,    91,
      91,    92,    92,    93,    93,    94,    94,    95,    95,    95,
      96,    96,    96,    97,    97,    97,    97,    97,    98,    98,
      98,    99,    99,   100,   100,   101,   101,   101,   101,   101,
     101,   101,   101,   101,   101,   102,   103,   103,   103,   103,
     104,   104,   105,   105
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     4,     3,     2,     3,     2,     6,     3,     2,
       2,     2,     1,     2,     1,     7,    10,     6,     9,     6,
       5,     5,     2,     1,     3,     2,     4,     1,     4,     1,
       3,     3,     1,     2,     3,     1,     1,     2,     2,     2,
       1,     5,     3,     1,     2,     1,     4,     4,     5,     5,
       3,     3,     5,     5,     4,     6,     6,     5,     3,     2,
       2,     2,     2,     2,     2,     3,     1,     1,     1,     1,
       1,     1,     3,     3,     1,     1,     4,     3,     4,     2,
       2,     3,     1,     3,     1,     3,     1,     3,     3,     1,
       3,     3,     1,     3,     3,     3,     3,     1,     3,     3,
       1,     3,     1,     3,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     2,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yytype], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyo, yytype, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[+yyssp[yyi + 1 - yynrhs]],
                       &yyvsp[(yyi + 1) - (yynrhs)]
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen(S) (YY_CAST (YYPTRDIFF_T, strlen (S)))
#  else
/* Return the length of YYSTR.  */
static YYPTRDIFF_T
yystrlen (const char *yystr)
{
  YYPTRDIFF_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYPTRDIFF_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYPTRDIFF_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (yyres)
    return yystpcpy (yyres, yystr) - yyres;
  else
    return yystrlen (yystr);
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYPTRDIFF_T *yymsg_alloc, char **yymsg,
                yy_state_t *yyssp, int yytoken)
{
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat: reported tokens (one for the "unexpected",
     one per "expected"). */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Actual size of YYARG. */
  int yycount = 0;
  /* Cumulated lengths of YYARG.  */
  YYPTRDIFF_T yysize = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[+*yyssp];
      YYPTRDIFF_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
      yysize = yysize0;
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYPTRDIFF_T yysize1
                    = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
                    yysize = yysize1;
                  else
                    return 2;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    /* Don't count the "%s"s in the final size, but reserve room for
       the terminator.  */
    YYPTRDIFF_T yysize1 = yysize + (yystrlen (yyformat) - 2 * yycount) + 1;
    if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
      yysize = yysize1;
    else
      return 2;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          ++yyp;
          ++yyformat;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss;
    yy_state_t *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYPTRDIFF_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYPTRDIFF_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
# undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2:
#line 104 "tinyGo.y"
                                 {
            assemblyFile.global = ".globl main";
            assemblyFile.data = ".data\n";
            assemblyFile.text = ".text\n";
            string code;

            list<Statement *>::iterator it = (yyvsp[0].statement_list_t)->begin();
            while(it != (yyvsp[0].statement_list_t)->end()){
                printf("semantic result: %d \n",(*it)->evaluateSemantic());
                code += (*it)->genCode();
                it++;
            }
            
            assemblyFile.text += code;
            writeFile("result.asm");

        }
#line 1638 "tinyGo_parser.cpp"
    break;

  case 3:
#line 121 "tinyGo.y"
                           {
            assemblyFile.global = ".globl main";
            assemblyFile.data = ".data\n";
            assemblyFile.text = ".text\n";
            string code;

            list<Statement *>::iterator it = (yyvsp[0].statement_list_t)->begin();
            while(it != (yyvsp[0].statement_list_t)->end()){
                printf("semantic result: %d \n",(*it)->evaluateSemantic());
                code += (*it)->genCode();
                it++;
            }
            
            assemblyFile.text += code;
            writeFile("result.s");
            
        }
#line 1660 "tinyGo_parser.cpp"
    break;

  case 5:
#line 143 "tinyGo.y"
                             { (yyval.import_t)->value.push_back((yyvsp[-1].string_value_t));}
#line 1666 "tinyGo_parser.cpp"
    break;

  case 6:
#line 144 "tinyGo.y"
                     { ImportList* list = new ImportList; list->push_back((yyvsp[-1].string_value_t));
                        (yyval.import_t) = new Import(*list,yylineno);}
#line 1673 "tinyGo_parser.cpp"
    break;

  case 7:
#line 146 "tinyGo.y"
                                                                                      {(yyval.import_t) = new Import(*(yyvsp[-2].import_list_t),yylineno);}
#line 1679 "tinyGo_parser.cpp"
    break;

  case 8:
#line 150 "tinyGo.y"
                    {
                       StringValue* val = new StringValue((yyvsp[-2].string_t),yylineno);
                        (yyval.import_list_t) = (yyvsp[0].import_list_t); (yyval.import_list_t)->push_back(val);
                    }
#line 1688 "tinyGo_parser.cpp"
    break;

  case 9:
#line 155 "tinyGo.y"
                  {
                      StringValue* val = new StringValue((yyvsp[-1].string_t),yylineno);
                      (yyval.import_list_t) = new ImportList; (yyval.import_list_t)->push_back(val);
                  }
#line 1697 "tinyGo_parser.cpp"
    break;

  case 10:
#line 161 "tinyGo.y"
                                {(yyval.string_value_t) = new StringValue((yyvsp[0].string_t),yylineno);}
#line 1703 "tinyGo_parser.cpp"
    break;

  case 11:
#line 164 "tinyGo.y"
                                  {(yyval.statement_list_t) = (yyvsp[-1].statement_list_t); (yyval.statement_list_t)->push_back((yyvsp[0].statement_t));}
#line 1709 "tinyGo_parser.cpp"
    break;

  case 12:
#line 165 "tinyGo.y"
                           {(yyval.statement_list_t) = new StatementList; (yyval.statement_list_t)->push_back((yyvsp[0].statement_t));}
#line 1715 "tinyGo_parser.cpp"
    break;

  case 13:
#line 168 "tinyGo.y"
                                            {(yyval.statement_t) = (yyvsp[-1].statement_t);}
#line 1721 "tinyGo_parser.cpp"
    break;

  case 14:
#line 169 "tinyGo.y"
                          {(yyval.statement_t) = new GlobalDeclaration((yyvsp[0].declaration_t));}
#line 1727 "tinyGo_parser.cpp"
    break;

  case 15:
#line 173 "tinyGo.y"
                                                                                                                    {
                    (yyval.statement_t) = new MethodDefinition((Type)(yyvsp[-1].int_t), (yyvsp[-5].string_t), *(yyvsp[-3].parameter_list_t), (yyvsp[0].statement_t), false, 0,yylineno);
                    delete (yyvsp[-3].parameter_list_t);
                 }
#line 1736 "tinyGo_parser.cpp"
    break;

  case 16:
#line 177 "tinyGo.y"
                                                                                                                                                                {
                    (yyval.statement_t) = new MethodDefinition((Type)(yyvsp[-1].int_t), (yyvsp[-8].string_t), *(yyvsp[-6].parameter_list_t), (yyvsp[0].statement_t), true, (yyvsp[-3].int_t),yylineno);
                    delete (yyvsp[-6].parameter_list_t);
                 }
#line 1745 "tinyGo_parser.cpp"
    break;

  case 17:
#line 181 "tinyGo.y"
                                                                                               {
                     ParameterList * pm = new ParameterList;
                     (yyval.statement_t) = new MethodDefinition((Type)(yyvsp[-1].int_t), (yyvsp[-4].string_t), *pm, (yyvsp[0].statement_t), false, 0, yylineno );
                     delete pm;
                 }
#line 1755 "tinyGo_parser.cpp"
    break;

  case 18:
#line 186 "tinyGo.y"
                                                                                                                                           {
                     ParameterList * pm = new ParameterList;
                     (yyval.statement_t) = new MethodDefinition((Type)(yyvsp[-1].int_t), (yyvsp[-7].string_t), *pm, (yyvsp[0].statement_t), true, (yyvsp[-3].int_t), yylineno );
                     delete pm;
                 }
#line 1765 "tinyGo_parser.cpp"
    break;

  case 19:
#line 191 "tinyGo.y"
                                                                                                               {
                    (yyval.statement_t) = new MethodDefinition(VOID, (yyvsp[-4].string_t), *(yyvsp[-2].parameter_list_t), (yyvsp[0].statement_t), false, 0,yylineno);
                    delete (yyvsp[-2].parameter_list_t);
                 }
#line 1774 "tinyGo_parser.cpp"
    break;

  case 20:
#line 195 "tinyGo.y"
                                                                                          {
                     ParameterList * pm = new ParameterList;
                     (yyval.statement_t) = new MethodDefinition(VOID, (yyvsp[-3].string_t), *pm, (yyvsp[0].statement_t), false, 0, yylineno );
                     delete pm;
                 }
#line 1784 "tinyGo_parser.cpp"
    break;

  case 21:
#line 200 "tinyGo.y"
                                                                                            {
                     ParameterList * pm = new ParameterList;
                     (yyval.statement_t) = new MethodDefinition(VOID, "main", *pm, (yyvsp[0].statement_t), false, 0, yylineno );
                     delete pm;
                 }
#line 1794 "tinyGo_parser.cpp"
    break;

  case 22:
#line 207 "tinyGo.y"
                                               { (yyval.declaration_list_t) = (yyvsp[-1].declaration_list_t); (yyval.declaration_list_t)->push_back((yyvsp[0].declaration_t)); }
#line 1800 "tinyGo_parser.cpp"
    break;

  case 23:
#line 208 "tinyGo.y"
                              {(yyval.declaration_list_t) = new DeclarationList; (yyval.declaration_list_t)->push_back((yyvsp[0].declaration_t));}
#line 1806 "tinyGo_parser.cpp"
    break;

  case 24:
#line 211 "tinyGo.y"
                                        { (yyval.declaration_t) = new Declaration((yyvsp[-1].init_declarator_t), yylineno);  }
#line 1812 "tinyGo_parser.cpp"
    break;

  case 25:
#line 214 "tinyGo.y"
                                 {(yyval.init_declarator_t) = new InitDeclarator((yyvsp[-1].declarator_t), (Type)(yyvsp[0].int_t), NULL, yylineno);}
#line 1818 "tinyGo_parser.cpp"
    break;

  case 26:
#line 215 "tinyGo.y"
                                                       { (yyval.init_declarator_t) = new InitDeclarator((yyvsp[-3].declarator_t), (Type)(yyvsp[-2].int_t) ,(yyvsp[0].initializer_t), yylineno); }
#line 1824 "tinyGo_parser.cpp"
    break;

  case 27:
#line 218 "tinyGo.y"
                   {(yyval.declarator_t) = new Declarator((yyvsp[0].string_t), 0, false, yylineno);}
#line 1830 "tinyGo_parser.cpp"
    break;

  case 28:
#line 219 "tinyGo.y"
                                                              { (yyval.declarator_t) = new Declarator((yyvsp[-3].string_t), (yyvsp[-1].int_t), true, yylineno);}
#line 1836 "tinyGo_parser.cpp"
    break;

  case 29:
#line 222 "tinyGo.y"
                                   {
                                        InitializerElementList * list = new InitializerElementList;
                                        list->push_back((yyvsp[0].expr_t));
                                        (yyval.initializer_t) = new Initializer(*list, yylineno);
                                    }
#line 1846 "tinyGo_parser.cpp"
    break;

  case 30:
#line 227 "tinyGo.y"
                                                       { (yyval.initializer_t) = new Initializer(*(yyvsp[-1].initializer_list_t), yylineno); delete (yyvsp[-1].initializer_list_t);  }
#line 1852 "tinyGo_parser.cpp"
    break;

  case 31:
#line 230 "tinyGo.y"
                                                                          {(yyval.parameter_list_t) = (yyvsp[-2].parameter_list_t); (yyval.parameter_list_t)->push_back((yyvsp[0].parameter_t));}
#line 1858 "tinyGo_parser.cpp"
    break;

  case 32:
#line 231 "tinyGo.y"
                                           { (yyval.parameter_list_t) = new ParameterList; (yyval.parameter_list_t)->push_back((yyvsp[0].parameter_t)); }
#line 1864 "tinyGo_parser.cpp"
    break;

  case 33:
#line 234 "tinyGo.y"
                                       { (yyval.parameter_t) = new Parameter((Type)(yyvsp[0].int_t),(yyvsp[-1].declarator_t), false, yylineno); }
#line 1870 "tinyGo_parser.cpp"
    break;

  case 34:
#line 237 "tinyGo.y"
                                                                  { (yyval.initializer_list_t) = (yyvsp[-2].initializer_list_t); (yyval.initializer_list_t)->push_back((yyvsp[0].expr_t)); }
#line 1876 "tinyGo_parser.cpp"
    break;

  case 35:
#line 238 "tinyGo.y"
                                         {(yyval.initializer_list_t) = new InitializerElementList; (yyval.initializer_list_t)->push_back((yyvsp[0].expr_t));}
#line 1882 "tinyGo_parser.cpp"
    break;

  case 36:
#line 241 "tinyGo.y"
                                {(yyval.statement_t) = (yyvsp[0].statement_t);}
#line 1888 "tinyGo_parser.cpp"
    break;

  case 37:
#line 242 "tinyGo.y"
                           {(yyval.statement_t) = (yyvsp[-1].statement_t);}
#line 1894 "tinyGo_parser.cpp"
    break;

  case 38:
#line 243 "tinyGo.y"
                            {(yyval.statement_t) = (yyvsp[-1].statement_t);}
#line 1900 "tinyGo_parser.cpp"
    break;

  case 39:
#line 244 "tinyGo.y"
                              {(yyval.statement_t) = (yyvsp[-1].statement_t);}
#line 1906 "tinyGo_parser.cpp"
    break;

  case 40:
#line 245 "tinyGo.y"
                         {(yyval.statement_t) = (yyvsp[0].statement_t);}
#line 1912 "tinyGo_parser.cpp"
    break;

  case 41:
#line 246 "tinyGo.y"
                                                                                 {(yyval.statement_t) = new PrintStatement(*(yyvsp[-2].expression_list_t), yylineno);}
#line 1918 "tinyGo_parser.cpp"
    break;

  case 42:
#line 249 "tinyGo.y"
                                                     {(yyval.expression_list_t) = (yyvsp[-2].expression_list_t);(yyval.expression_list_t)->push_back((yyvsp[0].expr_t));}
#line 1924 "tinyGo_parser.cpp"
    break;

  case 43:
#line 250 "tinyGo.y"
                            {(yyval.expression_list_t) = new ExpressionList;(yyval.expression_list_t)->push_back((yyvsp[0].expr_t));}
#line 1930 "tinyGo_parser.cpp"
    break;

  case 44:
#line 254 "tinyGo.y"
                                         { (yyval.statement_list_t) = (yyvsp[-1].statement_list_t); (yyval.statement_list_t)->push_back((yyvsp[0].statement_t)); }
#line 1936 "tinyGo_parser.cpp"
    break;

  case 45:
#line 255 "tinyGo.y"
                          { (yyval.statement_list_t) = new StatementList; (yyval.statement_list_t)->push_back((yyvsp[0].statement_t)); }
#line 1942 "tinyGo_parser.cpp"
    break;

  case 46:
#line 259 "tinyGo.y"
                                                             { 
                    DeclarationList * decls = new DeclarationList();
                    (yyval.statement_t) = new BlockStatement(*(yyvsp[-1].statement_list_t), *decls, yylineno);
                    delete decls;
               }
#line 1952 "tinyGo_parser.cpp"
    break;

  case 47:
#line 264 "tinyGo.y"
                                                               { 
                   StatementList * stmts = new StatementList();
                    (yyval.statement_t) = new BlockStatement(*stmts, *(yyvsp[-1].declaration_list_t), yylineno);
                    delete stmts;
               }
#line 1962 "tinyGo_parser.cpp"
    break;

  case 48:
#line 269 "tinyGo.y"
                                                                              {(yyval.statement_t) = new BlockStatement(*(yyvsp[-1].statement_list_t), *(yyvsp[-2].declaration_list_t), yylineno); delete (yyvsp[-1].statement_list_t); delete (yyvsp[-2].declaration_list_t); }
#line 1968 "tinyGo_parser.cpp"
    break;

  case 49:
#line 270 "tinyGo.y"
                                                                              {(yyval.statement_t) = new BlockStatement(*(yyvsp[-2].statement_list_t), *(yyvsp[-1].declaration_list_t), yylineno); delete (yyvsp[-1].declaration_list_t); delete (yyvsp[-2].statement_list_t); }
#line 1974 "tinyGo_parser.cpp"
    break;

  case 50:
#line 271 "tinyGo.y"
                                               {
                   StatementList * stmts = new StatementList();
                   DeclarationList * decls = new DeclarationList();
                   (yyval.statement_t) = new BlockStatement(*stmts, *decls, yylineno);
                   delete stmts;
                   delete decls;

               }
#line 1987 "tinyGo_parser.cpp"
    break;

  case 51:
#line 281 "tinyGo.y"
                                                {(yyval.statement_t) = new IfStatement((yyvsp[-1].expr_t),NULL, (yyvsp[0].statement_t), yylineno);}
#line 1993 "tinyGo_parser.cpp"
    break;

  case 52:
#line 282 "tinyGo.y"
                                                                        {(yyval.statement_t) = new ElseStatement((yyvsp[-3].expr_t),NULL, (yyvsp[-2].statement_t),(yyvsp[0].statement_t), yylineno);}
#line 1999 "tinyGo_parser.cpp"
    break;

  case 53:
#line 283 "tinyGo.y"
                                                                     {(yyval.statement_t) = new ElseIfStatement((yyvsp[-3].expr_t),NULL, (yyvsp[-2].statement_t),(yyvsp[0].statement_t), yylineno);}
#line 2005 "tinyGo_parser.cpp"
    break;

  case 54:
#line 284 "tinyGo.y"
                                                                          {(yyval.statement_t) = new IfStatement((yyvsp[-1].expr_t), (yyvsp[-2].statement_t),(yyvsp[0].statement_t), yylineno);}
#line 2011 "tinyGo_parser.cpp"
    break;

  case 55:
#line 285 "tinyGo.y"
                                                                                                 {(yyval.statement_t) = new ElseStatement((yyvsp[-3].expr_t), (yyvsp[-4].statement_t),(yyvsp[-2].statement_t),(yyvsp[0].statement_t), yylineno);}
#line 2017 "tinyGo_parser.cpp"
    break;

  case 56:
#line 286 "tinyGo.y"
                                                                                              {(yyval.statement_t) = new ElseIfStatement((yyvsp[-3].expr_t),(yyvsp[-4].statement_t), (yyvsp[-2].statement_t), (yyvsp[0].statement_t), yylineno);}
#line 2023 "tinyGo_parser.cpp"
    break;

  case 57:
#line 289 "tinyGo.y"
                                                                                                   {(yyval.statement_t) = new ForStatement((yyvsp[-3].statement_t),(yyvsp[-2].statement_t),(yyvsp[-1].expr_t),(yyvsp[0].statement_t),yylineno);}
#line 2029 "tinyGo_parser.cpp"
    break;

  case 58:
#line 290 "tinyGo.y"
                                                 {(yyval.statement_t) = new ForStatement(NULL,new ExprStatement((yyvsp[-1].expr_t),yylineno),NULL,(yyvsp[0].statement_t),yylineno);}
#line 2035 "tinyGo_parser.cpp"
    break;

  case 59:
#line 291 "tinyGo.y"
                                      {(yyval.statement_t) = new ForStatement(NULL,NULL,NULL,(yyvsp[0].statement_t),yylineno);}
#line 2041 "tinyGo_parser.cpp"
    break;

  case 60:
#line 294 "tinyGo.y"
                                                  {(yyval.statement_t) = new ExprStatement((yyvsp[-1].expr_t), yylineno);}
#line 2047 "tinyGo_parser.cpp"
    break;

  case 61:
#line 297 "tinyGo.y"
                                     {(yyval.statement_t) = new ExprStatement((yyvsp[-1].expr_t), yylineno);}
#line 2053 "tinyGo_parser.cpp"
    break;

  case 62:
#line 300 "tinyGo.y"
                              {(yyval.statement_t) = new ReturnStatement(NULL, yylineno);}
#line 2059 "tinyGo_parser.cpp"
    break;

  case 63:
#line 301 "tinyGo.y"
                                {(yyval.statement_t) = new ContinueStatement(yylineno);}
#line 2065 "tinyGo_parser.cpp"
    break;

  case 64:
#line 302 "tinyGo.y"
                             {(yyval.statement_t) = new BreakStatement(yylineno);}
#line 2071 "tinyGo_parser.cpp"
    break;

  case 65:
#line 303 "tinyGo.y"
                                         {(yyval.statement_t) = new ReturnStatement((yyvsp[-1].expr_t), yylineno);}
#line 2077 "tinyGo_parser.cpp"
    break;

  case 66:
#line 306 "tinyGo.y"
             {(yyval.int_t) = INT;}
#line 2083 "tinyGo_parser.cpp"
    break;

  case 67:
#line 307 "tinyGo.y"
               {(yyval.int_t) = FLOAT;}
#line 2089 "tinyGo_parser.cpp"
    break;

  case 68:
#line 308 "tinyGo.y"
              {(yyval.int_t) = BOOL;}
#line 2095 "tinyGo_parser.cpp"
    break;

  case 69:
#line 309 "tinyGo.y"
                {(yyval.int_t) = STRING;}
#line 2101 "tinyGo_parser.cpp"
    break;

  case 70:
#line 312 "tinyGo.y"
                          {(yyval.expr_t) = new IdExpr((yyvsp[0].string_t), yylineno);}
#line 2107 "tinyGo_parser.cpp"
    break;

  case 71:
#line 313 "tinyGo.y"
               {(yyval.expr_t) = (yyvsp[0].expr_t);}
#line 2113 "tinyGo_parser.cpp"
    break;

  case 72:
#line 314 "tinyGo.y"
                                                          {(yyval.expr_t) = (yyvsp[-1].expr_t);}
#line 2119 "tinyGo_parser.cpp"
    break;

  case 73:
#line 317 "tinyGo.y"
                                                                                    {(yyval.expr_t) = new AssignExpr((yyvsp[-2].expr_t),(yyvsp[0].expr_t),(yyvsp[-1].int_t),yylineno);}
#line 2125 "tinyGo_parser.cpp"
    break;

  case 74:
#line 318 "tinyGo.y"
                                             {(yyval.expr_t) = (yyvsp[0].expr_t);}
#line 2131 "tinyGo_parser.cpp"
    break;

  case 75:
#line 321 "tinyGo.y"
                                       {(yyval.expr_t) = (yyvsp[0].expr_t);}
#line 2137 "tinyGo_parser.cpp"
    break;

  case 76:
#line 322 "tinyGo.y"
                                                                                     { (yyval.expr_t) = new ArrayExpr((IdExpr*)(yyvsp[-3].expr_t), (yyvsp[-1].expr_t), yylineno); }
#line 2143 "tinyGo_parser.cpp"
    break;

  case 77:
#line 323 "tinyGo.y"
                                                                                  { (yyval.expr_t) = new MethodInvocationExpr((IdExpr*)(yyvsp[-2].expr_t), *(new ArgumentList), yylineno); }
#line 2149 "tinyGo_parser.cpp"
    break;

  case 78:
#line 324 "tinyGo.y"
                                                                                                           { (yyval.expr_t) = new MethodInvocationExpr((IdExpr*)(yyvsp[-3].expr_t), *(yyvsp[-1].argument_list_t), yylineno); }
#line 2155 "tinyGo_parser.cpp"
    break;

  case 79:
#line 325 "tinyGo.y"
                                                      { (yyval.expr_t) = new PostIncrementExpr((IdExpr*)(yyvsp[-1].expr_t), yylineno);}
#line 2161 "tinyGo_parser.cpp"
    break;

  case 80:
#line 326 "tinyGo.y"
                                                        { (yyval.expr_t) = new PostDecrementExpr((IdExpr*)(yyvsp[-1].expr_t), yylineno); }
#line 2167 "tinyGo_parser.cpp"
    break;

  case 81:
#line 330 "tinyGo.y"
                                                                                  {(yyval.argument_list_t) = (yyvsp[-2].argument_list_t);  (yyval.argument_list_t)->push_back((yyvsp[0].expr_t));}
#line 2173 "tinyGo_parser.cpp"
    break;

  case 82:
#line 331 "tinyGo.y"
                                                { (yyval.argument_list_t) = new ArgumentList; (yyval.argument_list_t)->push_back((yyvsp[0].expr_t));}
#line 2179 "tinyGo_parser.cpp"
    break;

  case 83:
#line 334 "tinyGo.y"
                                                                     { (yyval.expr_t) = new PercentExpr((yyvsp[-2].expr_t), (yyvsp[0].expr_t), yylineno); }
#line 2185 "tinyGo_parser.cpp"
    break;

  case 84:
#line 335 "tinyGo.y"
                                       {(yyval.expr_t) = (yyvsp[0].expr_t);}
#line 2191 "tinyGo_parser.cpp"
    break;

  case 85:
#line 338 "tinyGo.y"
                                                         { (yyval.expr_t) = new PowExpr((yyvsp[-2].expr_t), (yyvsp[0].expr_t), yylineno); }
#line 2197 "tinyGo_parser.cpp"
    break;

  case 86:
#line 339 "tinyGo.y"
                                   {(yyval.expr_t) = (yyvsp[0].expr_t);}
#line 2203 "tinyGo_parser.cpp"
    break;

  case 87:
#line 342 "tinyGo.y"
                                                                           { (yyval.expr_t) = new MulExpr((yyvsp[-2].expr_t), (yyvsp[0].expr_t), yylineno); }
#line 2209 "tinyGo_parser.cpp"
    break;

  case 88:
#line 343 "tinyGo.y"
                                                        { (yyval.expr_t) = new DivExpr((yyvsp[-2].expr_t), (yyvsp[0].expr_t), yylineno); }
#line 2215 "tinyGo_parser.cpp"
    break;

  case 89:
#line 344 "tinyGo.y"
                       {(yyval.expr_t) = (yyvsp[0].expr_t);}
#line 2221 "tinyGo_parser.cpp"
    break;

  case 90:
#line 347 "tinyGo.y"
                                                                           { (yyval.expr_t) = new AddExpr((yyvsp[-2].expr_t), (yyvsp[0].expr_t), yylineno); }
#line 2227 "tinyGo_parser.cpp"
    break;

  case 91:
#line 348 "tinyGo.y"
                                                                           { (yyval.expr_t) = new SubExpr((yyvsp[-2].expr_t), (yyvsp[0].expr_t), yylineno); }
#line 2233 "tinyGo_parser.cpp"
    break;

  case 92:
#line 349 "tinyGo.y"
                                                {(yyval.expr_t) = (yyvsp[0].expr_t);}
#line 2239 "tinyGo_parser.cpp"
    break;

  case 93:
#line 352 "tinyGo.y"
                                                                            { (yyval.expr_t) = new GtExpr((yyvsp[-2].expr_t), (yyvsp[0].expr_t), yylineno); }
#line 2245 "tinyGo_parser.cpp"
    break;

  case 94:
#line 353 "tinyGo.y"
                                                                         { (yyval.expr_t) = new LtExpr((yyvsp[-2].expr_t), (yyvsp[0].expr_t), yylineno); }
#line 2251 "tinyGo_parser.cpp"
    break;

  case 95:
#line 354 "tinyGo.y"
                                                                                     { (yyval.expr_t) = new GteExpr((yyvsp[-2].expr_t), (yyvsp[0].expr_t), yylineno); }
#line 2257 "tinyGo_parser.cpp"
    break;

  case 96:
#line 355 "tinyGo.y"
                                                                                  { (yyval.expr_t) = new LteExpr((yyvsp[-2].expr_t), (yyvsp[0].expr_t), yylineno); }
#line 2263 "tinyGo_parser.cpp"
    break;

  case 97:
#line 356 "tinyGo.y"
                                           {(yyval.expr_t) = (yyvsp[0].expr_t);}
#line 2269 "tinyGo_parser.cpp"
    break;

  case 98:
#line 359 "tinyGo.y"
                                                                               { (yyval.expr_t) = new EqExpr((yyvsp[-2].expr_t), (yyvsp[0].expr_t), yylineno); }
#line 2275 "tinyGo_parser.cpp"
    break;

  case 99:
#line 360 "tinyGo.y"
                                                                            { (yyval.expr_t) = new NeqExpr((yyvsp[-2].expr_t), (yyvsp[0].expr_t), yylineno); }
#line 2281 "tinyGo_parser.cpp"
    break;

  case 100:
#line 361 "tinyGo.y"
                                           {(yyval.expr_t) = (yyvsp[0].expr_t);}
#line 2287 "tinyGo_parser.cpp"
    break;

  case 101:
#line 364 "tinyGo.y"
                                                                          { (yyval.expr_t) = new LogicalOrExpr((yyvsp[-2].expr_t), (yyvsp[0].expr_t), yylineno); }
#line 2293 "tinyGo_parser.cpp"
    break;

  case 102:
#line 365 "tinyGo.y"
                                             {(yyval.expr_t) = (yyvsp[0].expr_t);}
#line 2299 "tinyGo_parser.cpp"
    break;

  case 103:
#line 368 "tinyGo.y"
                                                                          { (yyval.expr_t) = new LogicalAndExpr((yyvsp[-2].expr_t), (yyvsp[0].expr_t), yylineno); }
#line 2305 "tinyGo_parser.cpp"
    break;

  case 104:
#line 369 "tinyGo.y"
                                            {(yyval.expr_t) = (yyvsp[0].expr_t);}
#line 2311 "tinyGo_parser.cpp"
    break;

  case 105:
#line 373 "tinyGo.y"
                                       { (yyval.int_t) = COLONOREQUAL; }
#line 2317 "tinyGo_parser.cpp"
    break;

  case 106:
#line 374 "tinyGo.y"
                              { (yyval.int_t) = EQUAL; }
#line 2323 "tinyGo_parser.cpp"
    break;

  case 107:
#line 375 "tinyGo.y"
                                   {(yyval.int_t) = PLUSEQUAL; }
#line 2329 "tinyGo_parser.cpp"
    break;

  case 108:
#line 376 "tinyGo.y"
                                    { (yyval.int_t) = MINUSEQUAL; }
#line 2335 "tinyGo_parser.cpp"
    break;

  case 109:
#line 377 "tinyGo.y"
                                     { (yyval.int_t) = POTOREQUAL; }
#line 2341 "tinyGo_parser.cpp"
    break;

  case 110:
#line 378 "tinyGo.y"
                                     { (yyval.int_t) = DIVOREQUAL; }
#line 2347 "tinyGo_parser.cpp"
    break;

  case 111:
#line 379 "tinyGo.y"
                                     { (yyval.int_t) = MULTOREQUAL; }
#line 2353 "tinyGo_parser.cpp"
    break;

  case 112:
#line 380 "tinyGo.y"
                                         { (yyval.int_t) = PORCENTOREQUAL; }
#line 2359 "tinyGo_parser.cpp"
    break;

  case 113:
#line 381 "tinyGo.y"
                                 { (yyval.int_t) = OREQUAL; }
#line 2365 "tinyGo_parser.cpp"
    break;

  case 114:
#line 382 "tinyGo.y"
                                  { (yyval.int_t) = ANDEQUAL; }
#line 2371 "tinyGo_parser.cpp"
    break;

  case 115:
#line 385 "tinyGo.y"
                                  {(yyval.expr_t) = (yyvsp[0].expr_t);}
#line 2377 "tinyGo_parser.cpp"
    break;

  case 116:
#line 388 "tinyGo.y"
                     { (yyval.expr_t) = new IntExpr((yyvsp[0].int_t) , yylineno);}
#line 2383 "tinyGo_parser.cpp"
    break;

  case 117:
#line 389 "tinyGo.y"
                       { (yyval.expr_t) = new FloatExpr((yyvsp[0].float_t) , yylineno);}
#line 2389 "tinyGo_parser.cpp"
    break;

  case 118:
#line 390 "tinyGo.y"
                        { (yyval.expr_t) = new StringExpr((yyvsp[0].string_t) , yylineno);}
#line 2395 "tinyGo_parser.cpp"
    break;

  case 119:
#line 391 "tinyGo.y"
                  {(yyval.expr_t) = (yyvsp[0].expr_t);}
#line 2401 "tinyGo_parser.cpp"
    break;

  case 120:
#line 394 "tinyGo.y"
                 {(yyval.expr_t) = new BoolExpr((yyvsp[0].int_t) , yylineno);}
#line 2407 "tinyGo_parser.cpp"
    break;

  case 121:
#line 395 "tinyGo.y"
                   {(yyval.expr_t) = new BoolExpr((yyvsp[0].int_t) , yylineno);}
#line 2413 "tinyGo_parser.cpp"
    break;


#line 2417 "tinyGo_parser.cpp"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = YY_CAST (char *, YYSTACK_ALLOC (YY_CAST (YYSIZE_T, yymsg_alloc)));
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[+*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 401 "tinyGo.y"

