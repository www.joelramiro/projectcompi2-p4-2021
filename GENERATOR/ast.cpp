#include "ast.h"
#include <iostream>
#include <sstream>
#include <set>
#include <stack>
#include "asm.h"
using namespace std ;
class LabelFor
{
    public:
    LabelFor(
    string startForLabel,
    string endForLabel,
    string continueLabel)
    {
        this->startForLabel = startForLabel;
        this->continueLabel = continueLabel;
        this->endForLabel = endForLabel;
    };
    string startForLabel;
    string endForLabel;
    string continueLabel;
};

string globalDeclarationCode = "";
map<string,bool> globalVariablesInfo;
std::stack<LabelFor*> stackList;

extern Asm assemblyFile;

int globalStackPointer = 0;

class VariableInfo{
    public:
        VariableInfo(int offset, bool isArray, bool isParameter, Type type){
            this->offset = offset;
            this->isArray = isArray;
            this->isParameter = isParameter;
            this->type = type;
        }
        int offset;
        bool isArray;
        bool isParameter;
        Type type;
};

map<string, VariableInfo *> codeGenerationVars;
int labelCounter = 0;

const char * intTemps[] = {"$t0","$t1", "$t2","$t3","$t4","$t5","$t6","$t7","$t8","$t9" };
const char * floatTemps[] = {"$f0","$f1","$f2","$f3","$f4","$f5","$f6","$f7","$f8","$f9","$f10","$f11","$f12","$f13","$f14","$f15","$f16","$f17","$f18","$f19","$f20","$f21","$f22","$f23","$f24","$f25","$f26","$f27","$f28","$f29","$f30","$f31"};

bool findIntLabel(string word)
{
    for(int i = 0; i< 10;i++)
        if(intTemps[i] == word)
            return true;
    
    return false;
}

#define INT_TEMP_COUNT 10
#define FLOAT_TEMP_COUNT 32

set<string> intTempMap;
set<string> floatTempMap;    

string getIntTemp(){
    for (int i = 0; i < INT_TEMP_COUNT; i++)
    {
        if(intTempMap.find(intTemps[i]) == intTempMap.end()){
            intTempMap.insert(intTemps[i]);
            return string(intTemps[i]);
        }
    }
    cout<<"No more int registers!"<<endl;
    return "";
}

string getFloatTemp(){
    for (int i = 0; i < FLOAT_TEMP_COUNT; i++)
    {
        if(floatTempMap.find(floatTemps[i]) == floatTempMap.end()){
            floatTempMap.insert(floatTemps[i]);
            return string(floatTemps[i]);
        }
    }
    cout<<"No more float registers!"<<endl;
    return "";
}

void releaseIntTemp(string temp){
    intTempMap.erase(temp);
}

void releaseFloatTemp(string temp){
    floatTempMap.erase(temp);
}

void releaseRegister(string temp){
    releaseIntTemp(temp);
    releaseFloatTemp(temp);
}

string getNewLabel(string prefix){ //while, if
    stringstream ss;
    ss<<prefix << labelCounter; //while0, if1
    labelCounter++;
    return ss.str();
}




Type EnumOfIndex(int i) 
{
     return static_cast<Type>(i); 
}

string getTypeName(Type type){
    switch(type){
        case INT:
            return "INT";
        case FLOAT:
            return "FLOAT";
        case INT_ARRAY:
            return "INT_ARRAY";
        case FLOAT_ARRAY:
            return "FLOAT_ARRAY";
        case BOOL:
            return "BOOL";
        case VOID:
            return "VOID";
        case STRING:
            return "STRING";
    }

    cout<<"Unknown type"<<endl;
    exit(0);
}


Type getTypeByName(string type){

    if(type == "INT" )  
    {
        return INT;
    }else if(type == "FLOAT" )
    {
        return FLOAT;        
    }else if(type == "INT_ARRAY" )
    {
        return INT_ARRAY;        
    }else if(type == "FLOAT_ARRAY" )
    {
        return FLOAT_ARRAY;
    }else if(type == "BOOL")
    {
        return BOOL;
    }else if(type == "VOID")
    {
        return VOID;
    }else if(type == "STRING")
    {
        return STRING;        
    }

    cout<<"Unknown type"<<endl;
    exit(0);
}

string getKindName(StatementKind type)
{
    switch (type)
    {
        case WHILE_STATEMENT: return "WHILE_STATEMENT";
        case FOR_STATEMENT: return "FOR_STATEMENT";
        case IF_STATEMENT: return "IF_STATEMENT";
        case EXPRESSION_STATEMENT: return "EXPRESSION_STATEMENT";
        case RETURN_STATEMENT: return "RETURN_STATEMENT";
        case CONTINUE_STATEMENT: return "CONTINUE_STATEMENT";
        case BREAK_STATEMENT: return "BREAK_STATEMENT";
        case BLOCK_STATEMENT: return "BLOCK_STATEMENT";
        case PRINT_STATEMENT: return "PRINT_STATEMENT";
        case FUNCTION_DEFINITION_STATEMENT: return "FUNCTION_DEFINITION_STATEMENT";
        case GLOBAL_DECLARATION_STATEMENT: return "GLOBAL_DECLARATION_STATEMENT";
        case ELSE_STATEMENT: return "ELSE_STATEMENT";
        case ELSE_IF_STATEMENT: return "ELSE_IF_STATEMENT";
        default:return "NINGUNO ENCONTRO";
    }
    return "";
}

class ContextStack{
    public:
        struct ContextStack* prev;
        map<string, Type> variables;
        string methodParent;
        bool inFor;
};

class FunctionInfo{
    public:
        bool isArray;
        Type returnType;
        list<Parameter *> parameters;
};

map<string, Type> globalVariables = {};
map<string, Type> variables;
map<string, FunctionInfo*> methods;


void addMethodDeclaration(string id, int line, Type type, ParameterList params, bool isArray){
    if(methods[id] != 0){
        cout<<"redefinition of function "<<id<<" in line: "<<line<<endl;
        exit(0);
    }
    methods[id] = new FunctionInfo();
    methods[id]->returnType = type;
    methods[id]->isArray = isArray;
    methods[id]->parameters = params;
}

ContextStack * context = NULL;

void pushContext(string methodName, bool inFor){
    variables.clear();
    ContextStack * c = new ContextStack();
    c->variables = variables;
    c->prev = context;
    if (c->prev == NULL)
    {
        c->inFor = inFor;
        c->methodParent = methodName;
    }
    else
    {
        c->methodParent = c->prev->methodParent;
        if(c->prev->inFor)
        {
            c->inFor = true;
        }
        else
        {
            c->inFor = inFor;
        }
    }   
    
    context = c;
}

void popContext(){
    if(context != NULL){
        ContextStack * previous = context->prev;
        free(context);
        context = previous;
    }
}


Type getLocalVariableType(string id){
    ContextStack * currContext = context;
    while(currContext != NULL){
        if(currContext->variables[id] != 0)
            return currContext->variables[id];
        currContext = currContext->prev;
    }
    if(!context->variables.empty())
        return context->variables[id];
    return INVALID;
}


Type getVariableType(string id){
    if(!globalVariables.empty())
        return globalVariables[id];
    return INVALID;
}


bool variableExists(string id){
  Type value;
  if(context != NULL){
    value = getLocalVariableType(id);
    if(value != 0)
      return true;
  }
  return false;
}


Type MethodInvocationExpr::getType(){
    FunctionInfo * func = methods[this->id->id];
    if(func == NULL){
        cout<<"error: function "<<this->id->id<<" not found, line: "<<this->line<<endl;
        exit(0);
    }
    Type funcType = func->returnType;
    if(func->parameters.size() > this->args.size()){
        cout<<"error: to few arguments to function"<<this->id->id<<" line: "<<this->line<<endl;
        exit(0);
    }
    if(func->parameters.size() < this->args.size()){
        cout<<"error: to many arguments to function "<<this->id->id<<" line: "<<this->line<<endl;
        exit(0);
    }

    list<Parameter *>::iterator paramIt = func->parameters.begin();
    list<Expr *>::iterator argsIt =this->args.begin();
    while(paramIt != func->parameters.end() && argsIt != this->args.end()){
        string paramType = getTypeName((*paramIt)->type);
        if((*paramIt)->declarator->isArray)
        {
            paramType+="_ARRAY";
        }
        string argType = getTypeName((*argsIt)->getType());
        if( paramType != argType){
            cout<<"error: invalid conversion from: "<< argType <<" to " <<paramType<< " line: "<<this->line <<endl;
            exit(0);
        }
        paramIt++;
        argsIt++;
    }

    return funcType;
}

map<string, Type> resultTypes ={
    {"INT,INT", INT},
    {"FLOAT,FLOAT", FLOAT},
    {"INT,FLOAT", FLOAT},
    {"FLOAT,INT", FLOAT},
    {"BOOL,BOOL", BOOL},
    {"STRING,STRING", STRING},
};

bool canConvert(Type t1,Type t2)
{
    if (t1 == FLOAT)
    {
        if (t2 == INT || t2 == FLOAT)
        {
            return true;
        }
        
        return false;        
    }
    
    return t1 == t2;
}



Type IntExpr::getType(){
    return INT;
}

Type BoolExpr::getType(){
    return BOOL;
}

Type FloatExpr::getType(){
    return FLOAT;
}

Type PostIncrementExpr::getType(){
    return this->expr->getType();
}

Type PostDecrementExpr::getType(){
    return this->expr->getType();
}

Type ArrayExpr::getType(){
    return this->id->getType();
}

Type StringExpr::getType(){
    return STRING;
}






Type IdExpr::getType(){
    Type value;
    if(context != NULL){
        value = getLocalVariableType(this->id);
        if(value != 0)
            return value;
    }
    value = getVariableType(this->id);
    if(value == 0){
        cout<<"error: '"<<this->id<<"' was not declared in this scope. line: "<<this->line<<endl;
        exit(0);
    }
    return value;
}

int MethodDefinition::evaluateSemantic(){
    if(this->params.size() > 4){
        cout<< "Method: "<<this->id << " can't have more than 4 parameters, line: "<< this->line<<endl;
        exit(0);
    }
    
    addMethodDeclaration(this->id, this->line, this->type, this->params, this->isArray);
    pushContext(this->id,false);
   
    list<Parameter* >::iterator it = this->params.begin();
    while(it != this->params.end()){
        (*it)->evaluateSemantic();
        it++;
    }

    if(this->statement !=NULL ){
        this->statement->evaluateSemantic();
    }
    
    popContext();

    return 0;
}


int IfStatement::evaluateSemantic(){

    
    if (this->assignExpr != NULL)
    {
        this->assignExpr->evaluateSemantic();
    }

    if(this->conditionalExpr2 != NULL && this->conditionalExpr2->getType() != BOOL){
        cout<<"Expression for if must be boolean"<<endl;
        exit(0);
    }

    pushContext("",false);
    this->trueStatement->evaluateSemantic();
    popContext();
    return 0;
}


int ElseStatement::evaluateSemantic(){
    
    if (this->assignExpr != NULL)
    {
        this->assignExpr->evaluateSemantic();
    }
    
    if(this->conditionalExpr2 != NULL && this->conditionalExpr2->getType() != BOOL){
        cout<<"Expression for if must be boolean"<<endl;
        exit(0);
    }
    pushContext("",false);
    this->trueStatement->evaluateSemantic();
    popContext();
    pushContext("",false);
    if(this->falseStatement != NULL)
        this->falseStatement->evaluateSemantic();
    popContext();
    return 0;
}


int ElseIfStatement::evaluateSemantic()
{
   
    if (this->assignExpr != NULL)
    {
        this->assignExpr->evaluateSemantic();
    }
    
    if(this->conditionalExpr2 != NULL && this->conditionalExpr2->getType() != BOOL){
        cout<<"Expression for if must be boolean"<<endl;
        exit(0);
    }
    pushContext("",false);
    this->trueStatement->evaluateSemantic();
    popContext();
    pushContext("",false);
    if(this->otherStatement != NULL)
        this->otherStatement->evaluateSemantic();
    popContext();
    return 0;

}

int Parameter::evaluateSemantic(){
    if(!variableExists(this->declarator->id)){
        {
            if(this->declarator->isArray)
            {
                Type t = getTypeByName(getTypeName(this->type) + "_ARRAY");
                context->variables[declarator->id] = t;
            }
            else
            {
                context->variables[declarator->id] = this->type;
            }
        }
    }else{
        cout<<"error: redefinition of variable: "<< declarator->id<< " line: "<<this->line <<endl;
        exit(0);
    }
    return 0;
}

int BlockStatement::evaluateSemantic(){
    
    list<Declaration *>::iterator itd = this->declarations.begin();
    while (itd != this->declarations.end())
    {
        Declaration * dec = *itd;
        if(dec != NULL){
            dec->evaluateSemantic();
        }

        itd++;
    }

    list<Statement *>::iterator its = this->statements.begin();
    while (its != this->statements.end())
    {
        Statement * stmt = *its;
        if(stmt != NULL){
            stmt->evaluateSemantic();
        }
        its++;
    }

    return 0;
}

int ForStatement::evaluateSemantic()
{
    if (this->assignExpr != NULL)
    {
        this->assignExpr->evaluateSemantic();
    }
    
    if (this->conditionalExpr != NULL)
    {        
        ExprStatement* conditional = dynamic_cast<ExprStatement*>(conditionalExpr);

        if (conditional->expr->getType() != BOOL)
        {
            cout<<"Expression for FOR must be boolean"<<endl;
            exit(0);
        }
        
        this->conditionalExpr->evaluateSemantic();        
    }
    
    if (this->statement != NULL)    
    {
        pushContext("",true);
        this->statement->evaluateSemantic();
        popContext();
    }
    
    return 0;
}

int PrintStatement::evaluateSemantic()
{
    return this->exprs.front()->getType();
}

int ReturnStatement::evaluateSemantic()
{
    string methodName = context->methodParent;
    FunctionInfo* f = methods[methodName];
    
    if(f->isArray)
    {
        Type t = getTypeByName(getTypeName(f->returnType) + "_ARRAY");
        bool result = canConvert(t, this->expr->getType());
        if (!result)
        {
            cout<<"error: invalid conversion from: "<< getTypeName(this->expr->getType()) <<" to " <<getTypeName(t)<< " line: "<<this->line <<endl;
            exit(0);
        }
    }
    else
    {
        bool result = canConvert(f->returnType, this->expr->getType());
        if (!result)
        {
            cout<<"error: invalid conversion from: "<< getTypeName(this->expr->getType()) <<" to " <<getTypeName(f->returnType)<< " line: "<<this->line <<endl;
            exit(0);
        }
    }

    return 0;
}

int ContinueStatement::evaluateSemantic()
{
    if(!context->inFor)
    {
        cout<<"error: Invalid instruction. line: "<<this->line <<endl;
        exit(0);
    }

    //for
    return 0;
}

int ExprStatement::evaluateSemantic()
{
    this->expr->evaluateSemantic();
    return 0;
}

int BreakStatement::evaluateSemantic()
{
    if(!context->inFor)
    {
        cout<<"error: Invalid instruction. line: "<<this->line <<endl;
        exit(0);
    }

    return 0;
}

int GlobalDeclaration::evaluateSemantic()
{
    if (this->declaration == NULL)
    {
        cout<<"Declaration can't be NULL."<<endl;
        exit(0);
    }

    if(this->declaration->initDeclarator == NULL)
    {
        cout<<"Init declarator can't be NULL."<<endl;
        exit(0);
    }

    Type originalType = this->declaration->initDeclarator->type; 
    string id = this->declaration->initDeclarator->declarator->id;

    if(globalVariables[id] != 0)
    {
        cout<<"error: redefinition of variable: "<< id << " line: "<<this->line <<endl;
        exit(0);
    }

    
    if (this->declaration->initDeclarator->declarator->isArray)
    {
        int size = this->declaration->initDeclarator->declarator->sizeArray;
        if (size <= 0)
        {
            cout<<"error: array size is not valid. line: "<<this->line <<endl;
            exit(0);
        }
        
        if(this->declaration->initDeclarator->initializer == NULL)
        {
            globalVariables[id] = originalType;
        }
        else
        {
            list<Expr *>::iterator ite = this->declaration->initDeclarator->initializer->expressions.begin();
            int size2 = this->declaration->initDeclarator->initializer->expressions.size();
            if (size<size2)
            {
                cout<<"error: index out of range. line: "<<this->line <<endl;
                exit(0);
            }
            
            while(ite!=  this->declaration->initDeclarator->initializer->expressions.end())
            {
                Type exprType = (*ite)->getType();
                bool result = canConvert(originalType,exprType);
                if(!result)
                {
                    cout<<"error: invalid conversion from: "<< getTypeName(exprType) <<" to " <<getTypeName(originalType)<< " line: "<<this->line <<endl;
                    exit(0);
                }
                ite++;
            }

             globalVariables[id] = originalType;
        }
    }
    else
    {
        if(this->declaration->initDeclarator->initializer == NULL)
        {
            globalVariables[id] = originalType;
        }
        else
        {
            int sizeExpr = this->declaration->initDeclarator->initializer->expressions.size();
            if(sizeExpr != 1)
            {
                cout<<"error: too many expressions. Line:"<<this->line <<endl;
                exit(0);
            }
            else
            {
                Type exprType = this->declaration->initDeclarator->initializer->expressions.front()->getType();
                bool result = canConvert(originalType,exprType);
                if(!result)
                {
                    cout<<"error: invalid conversion from: "<< getTypeName(exprType) <<" to " <<getTypeName(originalType)<< " line: "<<this->line <<endl;
                    exit(0);
                }
                globalVariables[id] = originalType;
            }
        }
    }
    return 0;
}

int AssignExpr::evaluateSemantic()
{
    if (this->type == 8)
    {
        if(IdExpr* d = dynamic_cast<IdExpr*>(expr1))
        {
            if (!variableExists(d->id))
            {
                context->variables[d->id] = expr2->getType();
            }
            else
            {
                cout<<"error: redefinition of variable: "<< d->id << " line: "<<this->line <<endl;
                exit(0);
            }
        }
    }

    this->expr2->evaluateSemantic();
    return 0;
}

int Declaration::evaluateSemantic()
{
    if (this->initDeclarator == NULL)
    {
        cout<<"Init declarator can't be NULL."<<endl;
        exit(0);
    }

    Type originalType = this->initDeclarator->type; 
    string id = this->initDeclarator->declarator->id;

    if(variableExists(id))
    {
        cout<<"error: redefinition of variable: "<< id << " line: "<<this->line <<endl;
        exit(0);
    }

    if (this->initDeclarator->declarator->isArray)
    {
        int size = this->initDeclarator->declarator->sizeArray;
        if (size <= 0)
        {
            cout<<"error: array size is not valid. line: "<<this->line <<endl;
            exit(0);
        }
        
        if(this->initDeclarator->initializer == NULL)
        {
            Type t = getTypeByName(getTypeName(originalType) + "_ARRAY");
            context->variables[id] = t;
        }
        else
        {
            list<Expr *>::iterator ite = this->initDeclarator->initializer->expressions.begin();
            int size2 = this->initDeclarator->initializer->expressions.size();
            if (size<size2)
            {
                cout<<"error: index out of range. line: "<<this->line <<endl;
                exit(0);
            }
            
            while(ite!=  this->initDeclarator->initializer->expressions.end())
            {
                Type exprType = (*ite)->getType();
                bool result = canConvert(originalType,exprType);
                if(!result)
                {
                    cout<<"error: invalid conversion from: "<< getTypeName(exprType) <<" to " <<getTypeName(originalType)<< " line: "<<this->line <<endl;
                    exit(0);
                }
                ite++;
            }
            
             Type t = getTypeByName(getTypeName(originalType) + "_ARRAY");
             context->variables[id] = t;
        }
    }
    else
    {
        if(this->initDeclarator->initializer == NULL)
        {
            context->variables[id] = originalType;
        }
        else
        {
            int sizeExpr = this->initDeclarator->initializer->expressions.size();
            if(sizeExpr != 1)
            {
                cout<<"error: too many expressions. Line:"<<this->line <<endl;
                exit(0);
            }
            else
            {
                Type exprType = this->initDeclarator->initializer->expressions.front()->getType();
                bool result = canConvert(originalType,exprType);
                if(!result)
                {
                    cout<<"error: invalid conversion from: "<< getTypeName(exprType) <<" to " <<getTypeName(originalType)<< " line: "<<this->line <<endl;
                    exit(0);
                }
                context->variables[id] = originalType;
            }
        }
    }
    return 0;
}

int ArrayExpr::evaluateSemantic()
{
    if(this->id != NULL)
    {
        this->id->evaluateSemantic();
    }
    if(this->expr != NULL)
    {
        this->expr->evaluateSemantic();
    }
    return 0;
}


#define IMPLEMENT_BINARY_EVALUATE_SEMANTIC(name)\
int name##Expr::evaluateSemantic(){\
        return this->getType(); \
}\


#define IMPLEMENT_BINARY_GET_TYPE(name)\
Type name##Expr::getType(){\
    string leftType = getTypeName(this->expr1->getType());\
    string rightType = getTypeName(this->expr2->getType());\
    Type resultType = resultTypes[leftType+","+rightType];\
    if(resultType == 0){\
        cerr<< "Error: type "<< leftType <<" can't be converted to type "<< rightType <<" line: "<<this->line<<endl;\
        exit(0);\
    }\
    return resultType; \
}\

#define IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(name)\
Type name##Expr::getType(){\
    string leftType = getTypeName(this->expr1->getType());\
    string rightType = getTypeName(this->expr2->getType());\
    Type resultType = resultTypes[leftType+","+rightType];\
    if(leftType != rightType){\
        cerr<< "Error: type "<< leftType <<" can't be converted to type "<< rightType <<" line: "<<this->line<<endl;\
        exit(0);\
    }\
    return BOOL; \
}\

IMPLEMENT_BINARY_GET_TYPE(Add);
IMPLEMENT_BINARY_GET_TYPE(Sub);
IMPLEMENT_BINARY_GET_TYPE(Mul);
IMPLEMENT_BINARY_GET_TYPE(Div);
IMPLEMENT_BINARY_GET_TYPE(Assign);
IMPLEMENT_BINARY_GET_TYPE(Percent);

IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(Eq);
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(Neq);
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(Gte); 
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(Lte);
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(Gt);
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(Lt);
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(LogicalAnd);
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(LogicalOr);
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(Pow);

IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Id);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Add);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Sub);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Mul);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Div);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Eq);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Neq);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Gte);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Lte);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Gt);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Lt);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(LogicalAnd);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(LogicalOr);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Percent);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Pow);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Int);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Bool);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Float);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Binary);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(PostIncrement);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(PostDecrement);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(String);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(MethodInvocation);



// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< GENERACION DE CODIGO >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> //



void toFloat(Code &code){
    if(code.type == INT){
        string floatTemp = getFloatTemp();
        stringstream ss;
        ss << code.code
        << "mtc1 "<< code.place << ", " << floatTemp <<endl
        << "cvt.s.w " << floatTemp<< ", " << floatTemp <<endl;
        code.place = floatTemp;
        code.type = FLOAT;
        code.code =  ss.str();
        releaseRegister(code.place);
    }
    else{
        /* nothing */
    }
}

void IntExpr::genCode(Code &code,string labelName = ""){
    string temp = getIntTemp(); //$t0
    code.place = temp; // $t0
    stringstream ss;
    ss << "li " << temp <<", "<< this->value <<endl; // li $t0,5
    code.code = ss.str();
    code.type = INT;
}

void FloatExpr::genCode(Code &code,string labelName = ""){
    string floatTemp = getFloatTemp();//$f0
    code.place = floatTemp;//$f0
    stringstream ss;
    ss << "li.s " << floatTemp <<", "<< this->value <<endl; // li.s $t0,5.2
    code.code = ss.str();
    code.type = FLOAT;
}

void StringExpr::genCode(Code &code,string labelName = ""){    
    
    string strLabel;

    if (labelName == "")
    {
        strLabel = getNewLabel("string");
    }
    else
    {
        strLabel = labelName;
    }

    stringstream ss;
    ss << strLabel <<": .asciiz " << this->value << ""<<endl; // string0: .asciiz "test"
    assemblyFile.data += ss.str(); 
    code.code = "";
    code.place = strLabel; //string0
    code.type = STRING;
}

void BoolExpr::genCode(Code &code,string labelName = ""){    
    
    string temp = getIntTemp(); //$t0
    code.place = temp; // $t0
    stringstream ss;
    ss << "li " << temp <<", "<< this->value <<endl; // li $t0,5
    code.code = ss.str();
    code.type = BOOL;
}


string saveState(){
    set<string>::iterator it = floatTempMap.begin();
    stringstream ss;
    ss<<"sw $ra, " <<globalStackPointer<< "($sp)\n";
    globalStackPointer+=4;
    return ss.str();
}

string retrieveState(string state){
    std::string::size_type n = 0;
    string s = "sw";
    while ( ( n = state.find( s, n ) ) != std::string::npos )
    {
    state.replace( n, s.size(), "lw" );
    n += 2;
    globalStackPointer-=4;
    }
    return state;
}

map<string,bool> methodsData;
map<string,int> variablesArraysSize;
string MethodDefinition::genCode(){
    methodsData[this->id] = this->isArray;
    if(this->statement == NULL)
        return "";

    int stackPointer = 4;
    globalStackPointer = 0;
    stringstream code;
    code << this->id<<": "<<endl;
    if(this->id == "main")
    {
        code << globalDeclarationCode<<endl;
    }

    string state = saveState();
    code <<state<<endl;
    if(this->params.size() > 0){
        list<Parameter *>::iterator it = this->params.begin();
        for(int i = 0; i< this->params.size(); i++){
            code << "sw $a"<<i<<", "<< stackPointer<<"($sp)"<<endl;
            codeGenerationVars[(*it)->declarator->id] = new VariableInfo(stackPointer, (*it)->declarator->isArray, true, (*it)->type);
            stackPointer +=4;
            globalStackPointer +=4;
            it++;
        }
    }

    code<< this->statement->genCode()<<endl;
    stringstream sp;
    int currentStackPointer = globalStackPointer;
    sp << endl<<"addiu $sp, $sp, -"<<currentStackPointer<<endl;
    code << retrieveState(state);
    code << "addiu $sp, $sp, "<<currentStackPointer<<endl;
    code <<"jr $ra"<<endl;
    codeGenerationVars.clear();
    variablesArraysSize.clear();
    string result = code.str();
    result.insert(id.size() + 2, sp.str());
    return result;
}

void ArrayExpr::genCode(Code &code,string labelName = "")
{
    Code arrayCode;
    string name = this->id->id;
    stringstream ss;
    this->expr->genCode(arrayCode);
    //a[1]
    releaseRegister(arrayCode.place);
    if (codeGenerationVars.find(name) == codeGenerationVars.end())
    {
        string temp = getIntTemp();
        string labelAddress = getIntTemp();
        ss << arrayCode.code<<endl
        << "li $a0, 4"<<endl
        << "mult $a0, "<< arrayCode.place<<endl
        <<"mflo "<<temp<<endl
        << "la "<< labelAddress<<", "<< name<<endl
        << "add "<<temp<<", "<<labelAddress<<", "<<temp<<endl;
        releaseRegister(labelAddress);
        if(globalVariables[name] == INT  || globalVariables[name] == BOOL){
           ss <<"lw "<< temp<<", 0("<<temp<<")"<<endl;
           code.place = temp;
           code.type = INT;
        }else{
            string floatTemp = getFloatTemp();
            ss <<"l.s "<< floatTemp<<", 0("<<temp<<")"<<endl;
            code.place = floatTemp;
            code.type = FLOAT;
        }
    }else{
        string temp = getIntTemp();
        string address = getIntTemp();
        ss << arrayCode.code<<endl
        << "li $a0, 4"<<endl
        << "mult $a0, "<< arrayCode.place<<endl
        <<"mflo "<<temp<<endl;
        if(!codeGenerationVars[name]->isParameter)
            ss<< "la " + address +", " <<codeGenerationVars[name]->offset<<"($sp)\n";
        else
            ss << "lw "<<address<<", "<<codeGenerationVars[name]->offset<<"($sp)"<<endl;
        ss<< "add "<<temp<<", "<<address<<", "<<temp<<endl;
        releaseRegister(address);
        if(codeGenerationVars[name]->type == INT || codeGenerationVars[name]->type == BOOL){
           ss <<"lw "<< temp<<", 0("<<temp<<")"<<endl;
           code.place = temp;
           code.type = INT;
        }else{
            string floatTemp = getFloatTemp();
            ss <<"l.s "<< floatTemp<<", 0("<<temp<<")"<<endl;
           code.place = floatTemp;
           code.type = FLOAT;
        }
    }
    code.code = ss.str();
}

void MethodInvocationExpr::genCode(Code &code,string labelName = "")
{
    list<Expr *>::iterator it = this->args.begin();
    list<Code> codes;
    stringstream ss;
    Code argCode;
    int i = 0;
    bool isArray = false;
    while (it != this->args.end())
    {

        if(IdExpr* IDd = dynamic_cast<IdExpr*>((*it)))
        {
            isArray = codeGenerationVars[IDd->id]->isArray;
        }else
        {
            isArray = false;
        }

        (*it)->genCode(argCode);
        ss << argCode.code <<endl;
        codes.push_back(argCode);
        releaseRegister(argCode.place);

        if(isArray)
        {
            ss << "move $a"<<i<<", "<< argCode.place<<endl;
        }else if(argCode.type == FLOAT){
            ss << "mfc1 $a"<<i<<", "<< argCode.place<<endl;
        }else if (argCode.type == INT || argCode.type == BOOL){
            ss << "move $a"<<i<<", "<< argCode.place<<endl;
        }



        i++;
        it++;
    }

    
    ss<< "jal "<< this->id->id<<endl;
    string reg;
    if(methodsData[this->id->id])
    {
        reg = getIntTemp();
        ss << "move "<< reg<<", $v0"<<endl;
    }else if(methods[this->id->id]->returnType == FLOAT){
        reg = getFloatTemp();
        ss << "mtc1 $v0, "<< reg<<endl;
    }else{
        reg = getIntTemp();
        ss << "move "<< reg<<", $v0"<<endl;
    }
    code.code = ss.str();
    code.place = reg;
    code.type = methods[this->id->id]->returnType;
}


string ReturnStatement::genCode(){
    IdExpr* IDd = dynamic_cast<IdExpr*>(expr);
    
    bool isArray = false;
    if(codeGenerationVars[IDd->id] != 0)
    {
        isArray = codeGenerationVars[IDd->id]->isArray;
    }else
    {
        isArray = globalVariablesInfo[IDd->id];
    }

    Code exprCode;
    this->expr->genCode(exprCode);
    releaseRegister(exprCode.place);
    stringstream ss;
    ss << exprCode.code << endl;

    if(isArray)
    {
        ss<<"lw "<<exprCode.place<<", 0("<<exprCode.place<<")"<<endl;
        ss<< "move $v0, "<< exprCode.place <<endl;
    }else if(exprCode.type == INT || exprCode.type == BOOL)
    {
        ss<< "move $v0, "<< exprCode.place <<endl;
    }
    else if(exprCode.type == FLOAT)
    {
        ss<< "mfc1 $v0, "<<exprCode.place<<endl;
    }
    return ss.str();
}

void PostDecrementExpr::genCode(Code &code,string labelName = "")
{   
    Code leftCode;
    stringstream ss;
    this->expr->genCode(leftCode);
    if(leftCode.type == INT){
        code.type = INT;
        ss<< leftCode.code << endl;
        ss<<"addi "<<leftCode.place<<", "<<leftCode.place<<", -1"<<endl;
    }else{
        code.type = FLOAT;
        toFloat(leftCode);
        ss << leftCode.code << endl;
        string oneTemp = getFloatTemp();
        ss<<"li.s "<<oneTemp<<", 1.0"<<endl;
        ss<<"sub.s "<<leftCode.place<<", "<<leftCode.place<<", "<<oneTemp<<endl;
        releaseFloatTemp(oneTemp);
    }
    
    if(IdExpr* d = dynamic_cast<IdExpr*>(expr))
    {

        string name = d->id;
        VariableInfo* t =codeGenerationVars[d->id];
        if(name != ""){
            if(codeGenerationVars.find(name) == codeGenerationVars.end())
            {

                if(t->type == INT || t->type == BOOL)
                    ss << "sw "<<leftCode.place << ", "<<name <<endl;
                else if(t->type == FLOAT)
                    ss << "s.s "<<leftCode.place << ", "<<name <<endl;
            }
            else
            {
                if(t->type == INT || t->type == BOOL)
                    ss<< "sw "<< leftCode.place <<", "<<codeGenerationVars[name]->offset<<"($sp)"<<endl;
                else if(t->type == FLOAT)
                    ss<< "s.s "<< leftCode.place <<", "<<codeGenerationVars[name]->offset<<"($sp)"<<endl;
            
            }
        }
    }
    
    releaseRegister(leftCode.place);


    
    code.code = ss.str();
}

void PostIncrementExpr::genCode(Code &code,string labelName = "")
{
    Code leftCode;
    stringstream ss;
    this->expr->genCode(leftCode);
    if(leftCode.type == INT){
        code.type = INT;
        ss<< leftCode.code << endl;
        ss<<"addi "<<leftCode.place<<", "<<leftCode.place<<", 1"<<endl;
    }else{
        code.type = FLOAT;
        toFloat(leftCode);
        ss << leftCode.code << endl;
        string oneTemp = getFloatTemp();
        ss<<"li.s "<<oneTemp<<", 1.0"<<endl;
        ss<<"add.s "<<leftCode.place<<", "<<leftCode.place<<", "<<oneTemp<<endl;
        releaseFloatTemp(oneTemp);
    }
    
    if(IdExpr* d = dynamic_cast<IdExpr*>(expr))
    {

        string name = d->id;
        VariableInfo* t =codeGenerationVars[d->id];
        if(name != ""){
            if(codeGenerationVars.find(name) == codeGenerationVars.end())
            {

                if(t->type == INT || t->type == BOOL)
                    ss << "sw "<<leftCode.place << ", "<<name <<endl;
                else if(t->type == FLOAT)
                    ss << "s.s "<<leftCode.place << ", "<<name <<endl;
            }
            else
            {
                if(t->type == INT || t->type == BOOL)
                    ss<< "sw "<< leftCode.place <<", "<<codeGenerationVars[name]->offset<<"($sp)"<<endl;
                else if(t->type == FLOAT)
                    ss<< "s.s "<< leftCode.place <<", "<<codeGenerationVars[name]->offset<<"($sp)"<<endl;
            
            }
        }
    }
    
    releaseRegister(leftCode.place);
    code.code = ss.str();
}

void LogicalOrExpr::genCode(Code &code,string labelName = "") // 0-1
{
    Code leftSideCode; 
    Code rightSideCode;
    this->expr1->genCode(leftSideCode); //1, $t0
    this->expr2->genCode(rightSideCode); //0, $t1
    stringstream ss;
    ss<< leftSideCode.code<<endl << rightSideCode.code <<endl;
    string temp = getIntTemp();
    string label = getNewLabel("label");
    string finalLabel = getNewLabel("finalLabel");
    ss<< "addi "<<temp<< ", $zero, 1"<<endl; //temp = 1 
    ss<< "beq "<< leftSideCode.place<< ", "<<temp<<", "<< finalLabel<<endl; 
    ss<< "beq "<< rightSideCode.place<< ", "<<temp<<", "<< finalLabel<<endl;
    ss<< label<< ":"<<endl<< "addi "<< temp<< ", $zero, 0"<<endl<<finalLabel<<":"<<endl;
    code.place = temp; // 1
    code.code = ss.str();
    code.type = BOOL;
    releaseRegister(leftSideCode.place);
    releaseRegister(rightSideCode.place);
}

void LogicalAndExpr::genCode(Code &code,string labelName = "")
{ 
    Code leftSideCode; 
    Code rightSideCode;
    this->expr1->genCode(leftSideCode); //1
    this->expr2->genCode(rightSideCode); //1
    stringstream ss;
    ss<< leftSideCode.code<<endl << rightSideCode.code <<endl;
    string temp = getIntTemp();
    string label = getNewLabel("label");
    string finalLabel = getNewLabel("finalLabel");
    ss<< "addi "<<temp<< ", $zero, 0"<<endl; // temp 0
    ss<< "beq "<< leftSideCode.place<< ", "<<temp<<", "<< finalLabel<<endl;
    ss<< "beq "<< rightSideCode.place<< ", "<<temp<<", "<< finalLabel<<endl;
    ss<< label<< ":"<<endl<< "addi "<< temp<< ", $zero, 1"<<endl<<finalLabel<<":"<<endl;
    code.place = temp; // 1
    code.code = ss.str();
    code.type = BOOL;
    releaseRegister(leftSideCode.place);
    releaseRegister(rightSideCode.place);
}

void LtExpr::genCode(Code &code,string labelName = "")
{
    Code leftSideCode; 
    Code rightSideCode;
    stringstream ss;
    this->expr1->genCode(leftSideCode);
    this->expr2->genCode(rightSideCode);
    if (leftSideCode.type == INT && rightSideCode.type == INT)
    {
        code.type = INT;
        ss << leftSideCode.code <<endl<< rightSideCode.code<<endl;
        releaseRegister(leftSideCode.place);
        releaseRegister(rightSideCode.place);
        string temp = getIntTemp();
        ss<< "slt "<< temp<< ", "<< leftSideCode.place<< ", "<< rightSideCode.place<<endl;
        code.place = temp;
    }else{
        code.type = FLOAT;
        toFloat(leftSideCode);
        toFloat(rightSideCode);
        ss << leftSideCode.code << endl
        << rightSideCode.code <<endl;
        string temp = getIntTemp();
        string label = getNewLabel("label");
        string finalLabel = getNewLabel("finalLabel");
        ss << "c.lt.s " << leftSideCode.place << ", " << rightSideCode.place + "\n";
        ss << "bc1t " << label  + "\n";
        ss << "addi " << temp << ", $zero, 0"<<endl << " j " << finalLabel <<endl;
        ss<< label <<":"<<endl<< "addi " << temp << ", $zero, 1"<<endl<<finalLabel<<":"<<endl;
        code.place = temp;
        releaseRegister(leftSideCode.place);
        releaseRegister(rightSideCode.place);
    }
    code.type = BOOL;
    code.code = ss.str();
}

void GtExpr::genCode(Code &code,string labelName = "")
{
    Code leftSideCode; 
    Code rightSideCode;
    stringstream ss;
    this->expr1->genCode(leftSideCode);
    this->expr2->genCode(rightSideCode);
    if (leftSideCode.type == INT && rightSideCode.type == INT)
    {
        code.type = INT;
        ss<< leftSideCode.code <<endl
        << rightSideCode.code <<endl;
        string temp = getIntTemp();
        string label = getNewLabel("label");
        string finalLabel = getNewLabel("finalLabel");
        ss << "bgt " << leftSideCode.place << ", " << rightSideCode.place << ", " << label + "\n";
        ss << "addi " << temp << ", $zero, 0"<<endl << " j " << finalLabel <<endl;
        ss<< label <<":"<<endl<< "addi " << temp << ", $zero, 1"<<endl<<finalLabel<<":"<<endl;
        code.place = temp;
        releaseRegister(leftSideCode.place);
        releaseRegister(rightSideCode.place);
    }else{
        code.type = FLOAT;
        toFloat(leftSideCode);
        toFloat(rightSideCode);
        ss << leftSideCode.code << endl
        << rightSideCode.code <<endl;
        string temp = getIntTemp();
        string label = getNewLabel("label");
        string finalLabel = getNewLabel("finalLabel");
        ss << "c.le.s " << leftSideCode.place << ", " << rightSideCode.place + "\n"; // <= :1; >:0
        ss << "bc1f " << label  + "\n";
        ss << "addi " << temp << ", $zero, 0"<<endl << " j " << finalLabel <<endl;
        ss<< label <<":"<<endl<< "addi " << temp << ", $zero, 1"<<endl<<finalLabel<<":"<<endl;
        code.place = temp;
        releaseRegister(leftSideCode.place);
        releaseRegister(rightSideCode.place);
    }
    code.type = BOOL;
    code.code = ss.str();
}

void LteExpr::genCode(Code &code,string labelName = "")
{
    Code leftSideCode; 
    Code rightSideCode;
    stringstream ss;
    this->expr1->genCode(leftSideCode);
    this->expr2->genCode(rightSideCode);
    if (leftSideCode.type == INT && rightSideCode.type == INT)
    {
        code.type = INT;
        ss << leftSideCode.code <<endl<< rightSideCode.code<<endl;
        releaseRegister(leftSideCode.place);
        releaseRegister(rightSideCode.place);
        string temp = getIntTemp();
        ss<< "sle "<< temp<< ", "<< leftSideCode.place<< ", "<< rightSideCode.place<<endl; // Si es menor setea en 1, sino en cero. sle <destino>, <v1>, <v2>
        code.place = temp;
    }else{
        code.type = FLOAT;
        toFloat(leftSideCode);
        toFloat(rightSideCode);
        ss << leftSideCode.code << endl
        << rightSideCode.code <<endl;
        string temp = getIntTemp();
        string label = getNewLabel("label");
        string finalLabel = getNewLabel("finalLabel");
        ss << "c.le.s " << leftSideCode.place << ", " << rightSideCode.place + "\n"; // <= :1
        ss << "bc1t " << label  + "\n";
        ss << "addi " << temp << ", $zero, 0"<<endl << " j " << finalLabel <<endl;
        ss<< label <<":"<<endl<< "addi " << temp << ", $zero, 1"<<endl<<finalLabel<<":"<<endl;
        code.place = temp;
        releaseRegister(leftSideCode.place);
        releaseRegister(rightSideCode.place);
    }
    code.type = BOOL;
    code.code = ss.str();
}

void GteExpr::genCode(Code &code,string labelName = "")
{
    Code leftSideCode; 
    Code rightSideCode;
    stringstream ss;
    this->expr1->genCode(leftSideCode);
    this->expr2->genCode(rightSideCode);
    if (leftSideCode.type == INT && rightSideCode.type == INT)
    {
        code.type = INT;
        ss<< leftSideCode.code <<endl
        << rightSideCode.code <<endl;
        string temp = getIntTemp();
        string label = getNewLabel("label");
        string finalLabel = getNewLabel("finalLabel");
        ss << "bge " << leftSideCode.place << ", " << rightSideCode.place << ", " << label + "\n";
        ss << "addi " << temp << ", $zero, 0"<<endl << " j " << finalLabel <<endl;
        ss<< label <<":"<<endl<< "addi " << temp << ", $zero, 1"<<endl<<finalLabel<<":"<<endl;
        code.place = temp;
        releaseRegister(leftSideCode.place);
        releaseRegister(rightSideCode.place);
    }else{
        code.type = FLOAT;
        toFloat(leftSideCode);
        toFloat(rightSideCode);
        ss << leftSideCode.code << endl
        << rightSideCode.code <<endl;
        string temp = getIntTemp();
        string label = getNewLabel("label");
        string finalLabel = getNewLabel("finalLabel");
        ss << "c.lt.s " << leftSideCode.place << ", " << rightSideCode.place + "\n"; // QUE SEA MENOR. si es >=: = 0
        ss << "bc1f " << label  + "\n";
        ss << "addi " << temp << ", $zero, 0"<<endl << " j " << finalLabel <<endl;
        ss<< label <<":"<<endl<< "addi " << temp << ", $zero, 1"<<endl<<finalLabel<<":"<<endl;
        code.place = temp;
        releaseRegister(leftSideCode.place);
        releaseRegister(rightSideCode.place);
    }
    code.type = BOOL;
    code.code = ss.str();
}

void NeqExpr::genCode(Code &code,string labelName = "")
{
    Code leftSideCode; 
    Code rightSideCode;
    this->expr1->genCode(leftSideCode);
    this->expr2->genCode(rightSideCode);
    stringstream ss;
    if (leftSideCode.type == INT && rightSideCode.type == INT)
    {
        code.type = INT;
        ss<< leftSideCode.code <<endl
        << rightSideCode.code <<endl;
        string temp = getIntTemp();
        string label = getNewLabel("label");
        string finalLabel = getNewLabel("finalLabel");
        ss << "bne " << leftSideCode.place << ", " << rightSideCode.place << ", " << label + "\n";
        ss << "addi " << temp << ", $zero, 0"<<endl << " j " << finalLabel <<endl;
        ss<< label <<":"<<endl<< "addi " << temp << ", $zero, 1"<<endl<<finalLabel<<":"<<endl;
        code.place = temp;
        releaseRegister(leftSideCode.place);
        releaseRegister(rightSideCode.place);
    }else if(leftSideCode.type == FLOAT || rightSideCode.type == FLOAT ){
        code.type = FLOAT;
        toFloat(leftSideCode);
        toFloat(rightSideCode);
        ss << leftSideCode.code << endl
        << rightSideCode.code <<endl;
        string temp = getIntTemp();
        string label = getNewLabel("label");
        string finalLabel = getNewLabel("finalLabel");
        ss << "c.eq.s " << leftSideCode.place << ", " << rightSideCode.place + "\n";
        ss << "bc1f " << label  + "\n";
        ss << "addi " << temp << ", $zero, 0"<<endl << " j " << finalLabel <<endl;
        ss<< label <<":"<<endl<< "addi " << temp << ", $zero, 1"<<endl<<finalLabel<<":"<<endl;
        code.place = temp;
        releaseRegister(leftSideCode.place);
        releaseRegister(rightSideCode.place);
    }else if(leftSideCode.type == BOOL && rightSideCode.type == BOOL){
        code.type = BOOL;
        ss<< leftSideCode.code <<endl
        << rightSideCode.code <<endl;
        string temp = getIntTemp();
        string label = getNewLabel("label");
        string finalLabel = getNewLabel("finalLabel");
        ss << "bne " << leftSideCode.place << ", " << rightSideCode.place << ", " << label + "\n";
        ss << "addi " << temp << ", $zero, 0"<<endl << " j " << finalLabel <<endl;
        ss<< label <<":"<<endl<< "addi " << temp << ", $zero, 1"<<endl<<finalLabel<<":"<<endl;
        code.place = temp;
        releaseRegister(leftSideCode.place);
        releaseRegister(rightSideCode.place);
    }
    code.code = ss.str();
}

void EqExpr::genCode(Code &code,string labelName = ""){
    Code leftSideCode; 
    Code rightSideCode;
    this->expr1->genCode(leftSideCode);
    this->expr2->genCode(rightSideCode);
    stringstream ss;
    if (leftSideCode.type == INT && rightSideCode.type == INT)
    {
        code.type = INT;
        ss<< leftSideCode.code <<endl
        << rightSideCode.code <<endl;
        string temp = getIntTemp();
        string label = getNewLabel("label");
        string finalLabel = getNewLabel("finalLabel");
        ss << "beq " << leftSideCode.place << ", " << rightSideCode.place << ", " << label + "\n";
        ss << "addi " << temp << ", $zero, 0"<<endl << " j " << finalLabel <<endl;
        ss<< label <<":"<<endl<< "addi " << temp << ", $zero, 1"<<endl<<finalLabel<<":"<<endl;
        code.place = temp;
        releaseRegister(leftSideCode.place);
        releaseRegister(rightSideCode.place);
    }else if(leftSideCode.type == FLOAT || rightSideCode.type == FLOAT ){
        code.type = FLOAT;
        toFloat(leftSideCode);
        toFloat(rightSideCode);
        ss << leftSideCode.code << endl
        << rightSideCode.code <<endl;
        string temp = getIntTemp();
        string label = getNewLabel("label");
        string finalLabel = getNewLabel("finalLabel");
        ss << "c.eq.s " << leftSideCode.place << ", " << rightSideCode.place + "\n";
        ss << "bc1t " << label  + "\n";
        ss << "addi " << temp << ", $zero, 0"<<endl << " j " << finalLabel <<endl;
        ss<< label <<":"<<endl<< "addi " << temp << ", $zero, 1"<<endl<<finalLabel<<":"<<endl;
        code.place = temp;
        releaseRegister(leftSideCode.place);
        releaseRegister(rightSideCode.place);
    }else if(leftSideCode.type == BOOL && rightSideCode.type == BOOL)
    {
        code.type = BOOL;
        ss<< leftSideCode.code <<endl
        << rightSideCode.code <<endl;
        string temp = getIntTemp();
        string label = getNewLabel("label");
        string finalLabel = getNewLabel("finalLabel");
        ss << "beq " << leftSideCode.place << ", " << rightSideCode.place << ", " << label + "\n";
        ss << "addi " << temp << ", $zero, 0"<<endl << " j " << finalLabel <<endl;
        ss<< label <<":"<<endl<< "addi " << temp << ", $zero, 1"<<endl<<finalLabel<<":"<<endl;
        code.place = temp;
        releaseRegister(leftSideCode.place);
        releaseRegister(rightSideCode.place);
    }
    code.code = ss.str();
}


void AssignExpr::genCode(Code &code, string labelName = ""){
    stringstream ss;
    if(this->type == 8)
    {   
        IdExpr* idExpr = dynamic_cast<IdExpr*>(expr1);
        codeGenerationVars[idExpr->id] = new VariableInfo(globalStackPointer, false, false, expr2->getType());
        globalStackPointer +=4;
    }      //

    Code rightSideCode;
    this->expr2->genCode(rightSideCode);
    ss<< rightSideCode.code <<endl;
    string name;
    if(ArrayExpr* d = dynamic_cast<ArrayExpr*>(expr1))
    {
        name = d->id->id;
    }else 
    {
         name = ((IdExpr *)this->expr1)->id;
    }
    
    if(name != ""){
        if(codeGenerationVars.find(name) == codeGenerationVars.end()){
        if(rightSideCode.type == INT || rightSideCode.type == BOOL)
        {
            int number = 0;
            if(ArrayExpr* d = dynamic_cast<ArrayExpr*>(expr1))
            {
                IntExpr* intva = dynamic_cast<IntExpr*>(d->expr);
                number = intva->value;
            }
            string inttemp = getIntTemp();
            ss << "la "<<inttemp << ", "<<name <<endl; // posicion inicial del arreglo = 5500. => 5500+ 3*4 = 5512
            ss <<"addi "<<inttemp<<", "<<inttemp<<", "<<number*4<<endl;
            ss << "sw "<<rightSideCode.place << ",  0("<<inttemp<<")"<<endl;
            releaseRegister(inttemp);
        }
        else if(rightSideCode.type == FLOAT)
        {
            int number = 0;
            if(ArrayExpr* d = dynamic_cast<ArrayExpr*>(expr1))
            {
                IntExpr* intva = dynamic_cast<IntExpr*>(d->expr);
                number = intva->value;
            }
            string inttemp = getIntTemp();
            ss << "la "<<inttemp << ", "<<name <<endl; 
            ss <<"addi "<<inttemp<<", "<<inttemp<<", "<<number*4<<endl;            
            ss<< "s.s "+ rightSideCode.place + ", 0("<<inttemp<<")" <<endl;
            releaseRegister(inttemp);
        }
        }else{           
            bool isArray = false;
            if(MethodInvocationExpr* d = dynamic_cast<MethodInvocationExpr*>(expr2))
            {
                isArray = methodsData[d->id->id];
            }

            if(isArray)// PENDIENTE
            {
                int number = 0;
                if(IdExpr* d = dynamic_cast<IdExpr*>(expr1))
                {
                    number = variablesArraysSize[d->id];
                }
                    if(rightSideCode.type == INT || rightSideCode.type == BOOL)
                    {
                        string temp = getIntTemp();
                        for (int i = 0; i < number; i++)
                        {
                            int posRead = i*4;
                            ss<<"addi "<<temp<<", "<<"$v0, "<<posRead<<endl; // 6000+0,+4,+8
                            ss<< "lw "<< temp <<", 0("<<temp<<")"<<endl;
                            ss<< "sw "<< temp <<", "<<((codeGenerationVars[name]->offset)+posRead)<<"($sp)"<<endl;
                        }
                        releaseRegister(temp);
                    } else if(rightSideCode.type == FLOAT)
                    {
                        string temp = getFloatTemp();
                        string inttemp = getIntTemp();
                        for (int i = 0; i < number; i++)
                        {
                            int posRead = i*4;
                            ss<<"addi "<<inttemp<<", "<<"$v0, "<<posRead<<endl; // 6000+0,+4,+8
                            ss<< "l.s "<< temp <<", 0("<<inttemp<<")"<<endl;
                            ss<< "s.s "<< temp <<", "<<((codeGenerationVars[name]->offset)+posRead)<<"($sp)"<<endl;
                        }
                        releaseRegister(temp);
                        releaseRegister(inttemp);
                    }
                
                
                //ss<<
            }
            else if(rightSideCode.type == INT || rightSideCode.type == BOOL)
            {

                int number = 0;
                if(ArrayExpr* d = dynamic_cast<ArrayExpr*>(expr1))
                {
                    
                    if(IntExpr* intva = dynamic_cast<IntExpr*>(d->expr)) //ARRY[5]
                    {
                        number = intva->value;
                        ss<< "sw "<< rightSideCode.place <<", "<<((codeGenerationVars[name]->offset)+(number*4))<<"($sp)"<<endl;

                    }
                    else
                    {
                        Code temp;
                        d->expr->genCode(temp);
                        ss<<temp.code;
                        string t = getIntTemp();
                        ss<<"li "<<t<<", 4"<<endl;
                        ss<<"mult "<<temp.place<<", "<<t<<endl;
                        ss<<"mflo " <<temp.place<<endl; //5 * 4 = 20

                        if(codeGenerationVars[name]->isParameter)
                        {
                            ss<<"lw "<<t<<", "<<(codeGenerationVars[name]->offset)<<"($sp)"<<endl; // la direcccion a[] =>  sp => 6000 => a => 5000
                        }else
                        {
                            ss<<"move "<<t<<", $sp"<<endl;
                            ss<<"addi "<<t<<", "<<t<<", "<<(codeGenerationVars[name]->offset)<<endl; // a[] => 5000 => sp
                        }
                        ss<<"add "<<temp.place<<", "<<temp.place<<","<<t<<endl;
                        ss<< "sw "<< rightSideCode.place <<", 0("<<temp.place<<")"<<endl;

                        releaseRegister(temp.place);
                        releaseRegister(t);
                    }
                }
                else
                {
                    ss<< "sw "<< rightSideCode.place <<", "<<codeGenerationVars[name]->offset<<"($sp)"<<endl;
                }
            }
            else if(rightSideCode.type == FLOAT)
            {
                int number = 0;
                if(ArrayExpr* d = dynamic_cast<ArrayExpr*>(expr1))
                {
                    
                    if(IntExpr* intva = dynamic_cast<IntExpr*>(d->expr)) //ARRY[5]
                    {
                        number = intva->value;
                        ss<< "s.s "<< rightSideCode.place <<", "<<((codeGenerationVars[name]->offset)+(number*4))<<"($sp)"<<endl;

                    }
                    else
                    {
                        Code temp;
                        d->expr->genCode(temp);
                        ss<<temp.code;
                        string t = getIntTemp();
                        ss<<"li "<<t<<", 4"<<endl;
                        ss<<"mult "<<temp.place<<", "<<t<<endl;
                        ss<<"mflo " <<temp.place<<endl; //5 * 4 = 20
                        
                        if(codeGenerationVars[name]->isParameter)
                        {
                            ss<<"lw "<<t<<", "<<(codeGenerationVars[name]->offset)<<"($sp)"<<endl; // la direcccion a[] =>  sp => 6000 => a => 5000
                        }else
                        {
                            ss<<"move "<<t<<", $sp"<<endl;
                            ss<<"addi "<<t<<", "<<t<<", "<<(codeGenerationVars[name]->offset)<<endl; // a[] => 5000 => sp
                        }

                        ss<<"add "<<temp.place<<", "<<temp.place<<","<<t<<endl;
                        ss<< "s.s "<< rightSideCode.place <<", 0("<<temp.place<<")"<<endl;
                        releaseRegister(temp.place);
                        releaseRegister(t);
                    }
                }
                else
                {
                    ss<< "s.s "<< rightSideCode.place <<", "<<codeGenerationVars[name]->offset<<"($sp)"<<endl;
                }
            }
        }
        releaseRegister(rightSideCode.place);
    }else{
        Code arrayExpr;
        string name = ((ArrayExpr *)this->expr1)->id->id;
        ((ArrayExpr *)this->expr1)->expr->genCode(arrayExpr);
        releaseRegister(arrayExpr.place);
        ss << arrayExpr.code<<endl;
        string temp = getIntTemp();
        string address = getIntTemp();
        if(codeGenerationVars.find(name) == codeGenerationVars.end()){
            ss<< "li $a0, 4"<<endl
            << "mult $a0, "<< arrayExpr.place<<endl
            <<"mflo "<<temp<<endl
            << "la " <<address<< ", "<< name<<endl
            <<"add "<< temp << ", "<< temp << ", "<< address<<endl;
            Code rightSideCode;
            this->expr2->genCode(rightSideCode);
            ss<< rightSideCode.code <<endl
            << "sw "<< rightSideCode.place << ", 0(" <<temp<< ")"<<endl;
        }else{
            ss << "li $a0, 4"<<endl
            << "mult $a0, "<< arrayExpr.place<<endl
            <<"mflo "<<temp<<endl;
            if(!codeGenerationVars[name]->isParameter)
                ss<< "la " + address +", " <<codeGenerationVars[name]->offset<<"($sp)"<<endl;
            else
                ss << "lw "<<address<<", "<<codeGenerationVars[name]->offset<<"($sp)"<<endl;
            ss <<"add "<< temp << ", "<< temp << ", "<< address<<endl;
            Code rightSideCode;
            this->expr2->genCode(rightSideCode);
            ss<< rightSideCode.code <<endl;
            ss<< "sw "<< rightSideCode.place << ", 0("<<temp<<")"<<endl;
        }    
        releaseRegister(temp);     
        releaseRegister(address);     
        releaseRegister(rightSideCode.place);
        releaseRegister(arrayExpr.place);
    }
    code.code = ss.str();
}

void IdExpr::genCode(Code &code,string labelName = "")
{
    if(codeGenerationVars.find(this->id) == codeGenerationVars.end()){
        code.type = globalVariables[this->id];
        if(globalVariables[this->id] == INT_ARRAY || globalVariables[this->id] == FLOAT_ARRAY){
            string intTemp = getIntTemp();
            code.code = "la " + intTemp+ ", "+ this->id + "\n";
            code.place = intTemp;
        }else if(globalVariables[this->id] == INT || globalVariables[this->id] == BOOL){
            string intTemp = getIntTemp();
            code.place = intTemp;
            code.code = "lw "+ intTemp + ", "+ this->id + "\n";
        }else if(globalVariables[this->id] == FLOAT){
            string floatTemp = getFloatTemp();
            code.place = floatTemp;
            code.code = "l.s "+ floatTemp + ", "+ this->id + "\n";
        }else if(globalVariables[this->id] == STRING){
            code.place = this->id;
        }
    }
   else{
        code.type = codeGenerationVars[this->id]->type;
        if((codeGenerationVars[this->id]->type == INT || codeGenerationVars[this->id]->type == BOOL) && !codeGenerationVars[this->id]->isArray){
            string intTemp = getIntTemp();
            code.place = intTemp;
            code.code = "lw "+ intTemp + ", " + to_string(codeGenerationVars[this->id]->offset) +"($sp)\n";
        }else if(codeGenerationVars[this->id]->type == FLOAT && !codeGenerationVars[this->id]->isArray){
            string floatTemp = getFloatTemp();
            code.place = floatTemp;
            code.code = "l.s "+ floatTemp + ", " +to_string(codeGenerationVars[this->id]->offset) +"($sp)\n";
        }else if(codeGenerationVars[this->id]->isArray){
            string intTemp = getIntTemp();
            stringstream ss;
            ss<<"move "<<intTemp<<", $sp"<<endl; //inttemp = 5000
            ss<<"addi "<<intTemp<<","<<intTemp<<","<<codeGenerationVars[this->id]->offset<<endl; // 5000+4=5004
            code.code = ss.str();
            code.place = intTemp;
        }
    }
}

string Declaration::genCode(){
    stringstream code;
        InitDeclarator * initDeclarator = this->initDeclarator;
    
        if (!initDeclarator->declarator->isArray)
        {
            codeGenerationVars[initDeclarator->declarator->id] = new VariableInfo(globalStackPointer, false, false, initDeclarator->type);
            globalStackPointer +=4;
        }else{
            variablesArraysSize[initDeclarator->declarator->id] =initDeclarator->declarator->sizeArray;
            codeGenerationVars[initDeclarator->declarator->id] = new VariableInfo(globalStackPointer, true, false, initDeclarator->type);
            if(initDeclarator->initializer == NULL){
                int size = initDeclarator->declarator->sizeArray;
                globalStackPointer +=(size * 4);
            }
        }

        //int arr[] = {1,3,4,5}
        if(initDeclarator->initializer != NULL){
            list<Expr *>::iterator itExpr = initDeclarator->initializer->expressions.begin();
            int offset = codeGenerationVars[initDeclarator->declarator->id]->offset;
            for (int i = 0; i < initDeclarator->initializer->expressions.size(); i++)
            {
                Code exprCode;
                (*itExpr)->genCode(exprCode);
                code << exprCode.code <<endl;
                if(exprCode.type == INT || exprCode.type == BOOL)
                    code << "sw " << exprCode.place <<", "<< offset << "($sp)"<<endl;
                else if(exprCode.type == FLOAT)
                    code << "s.s " << exprCode.place <<", "<< offset << "($sp)"<<endl;

                releaseRegister(exprCode.place);
                itExpr++;
                if (initDeclarator->declarator->isArray)
                {
                    globalStackPointer+=4;
                    offset += 4; 
                }
            }
            
        }
    return code.str();
}


string GlobalDeclaration::genCode(){
    stringstream data;
    stringstream code;

        InitDeclarator * declaration = this->declaration->initDeclarator;
        globalVariablesInfo[declaration->declarator->id] = declaration->declarator->isArray;
        data << declaration->declarator->id <<": .word 0";
        if(declaration->initializer != NULL){
            list<Expr *>::iterator itExpr = declaration->initializer->expressions.begin();
            for(int i = 0; i < declaration->initializer->expressions.size(); i++){
                Code exprCode;
                (*itExpr)->genCode(exprCode);
                code << exprCode.code;
                if(!declaration->declarator->isArray){
                    if(exprCode.type == INT || exprCode.type == BOOL)
                        code << "sw "<< exprCode.place<< ", " << declaration->declarator->id<<endl;
                    else if(exprCode.type == FLOAT)
                        code << "s.s "<< exprCode.place<< ", " << declaration->declarator->id<<endl;
                }else{
                    string temp = getIntTemp();
                    code << "la "<<temp<<", "<<declaration->declarator->id<<endl;
                    if(exprCode.type == INT || exprCode.type == BOOL)
                    {
                        code <<"sw "<<exprCode.place<<", "<< i*4<<"("<<temp<<")"<<endl;
                    }else if(exprCode.type == FLOAT)
                    {
                        code <<"s.s "<<exprCode.place<<", "<< i*4<<"("<<temp<<")"<<endl;
                    }
                }
                releaseRegister(exprCode.place);
                itExpr++;
            }
        }else if(declaration->declarator->isArray){
            int size = declaration->declarator->sizeArray;
            for (int i = 0; i < size - 1; i++)
            {
                data<<", 0";
            }
        }
        data<<endl;
        

    assemblyFile.data += data.str();
    globalDeclarationCode += code.str();
    return "";
}

string BreakStatement::genCode(){
    stringstream data;
    LabelFor* value = stackList.top();
    data<<"j "<<value->endForLabel<<endl;
    return data.str();
}

string ExprStatement::genCode(){
    Code exprCode;
    this->expr->genCode(exprCode);
    releaseRegister(exprCode.place);
    return exprCode.code;
}

string ContinueStatement::genCode(){
    stringstream data;
    LabelFor* value = stackList.top();
    data<<"j "<<value->continueLabel<<endl;
    return data.str();
}

string PrintStatement::genCode(){
    list<Expr *>::iterator ite =  this->exprs.begin();
    stringstream code;

    while (ite != this->exprs.end())
    {
            Expr * expr = *ite;  
            Code exprCode;
            expr->genCode(exprCode);
            code<< exprCode.code<<endl;
            if(exprCode.type == INT || exprCode.type == BOOL){
                code <<"move $a0, "<< exprCode.place<<endl
                << "li $v0, 1"<<endl
                << "syscall"<<endl;
            }else if(exprCode.type == FLOAT){
                code << "mov.s $f12, "<< exprCode.place<<endl
                << "li $v0, 2"<<endl
                << "syscall"<<endl;
            }else if(exprCode.type == STRING){
                code << "la $a0, "<< exprCode.place<<endl // la $a0,stringtest
                << "li $v0, 4"<<endl
                << "syscall"<<endl;
            }
            
            releaseRegister(exprCode.place);
            ite++;
    }

    code <<"li $a0, 10"<<endl;
    code << "li $v0, 11"<<endl;
    code << "syscall"<<endl;
    
    return code.str();
}

string ForStatement::genCode()
{
    stringstream ss;
    if(this->assignExpr != NULL)
    {
        ss << this->assignExpr->genCode() << endl;
    }

    string forLabel = getNewLabel("for");
    string endForLabel = getNewLabel("endFor"); 
    string continueLabel = getNewLabel("continueFor");   
    stackList.push(new LabelFor(forLabel,endForLabel,continueLabel));
    

    ss << forLabel << ": "<< endl;
    Code code;

    if (conditionalExpr != NULL)
    {
        ExprStatement* conditional = dynamic_cast<ExprStatement*>(conditionalExpr);
        conditional->expr->genCode(code);
        releaseRegister(code.place);
        ss << code.code <<endl;
        ss << "beqz "<< code.place << ", " << endForLabel <<endl;
    }

    if(this->statement != NULL)
    {
        ss<< this->statement->genCode() <<endl;
    }

    ss<<continueLabel<<":"<<endl;
    if(this->expr != NULL)
    {
        this->expr->genCode(code);
        releaseRegister(code.place);
        ss<<code.code<<endl;
    }

    ss << "j " << forLabel <<endl
    << endForLabel << ": "<<endl;

    stackList.pop();
    return ss.str();
}

string BlockStatement::genCode(){
    
    stringstream ss;

    list<Declaration *>::iterator itd = this->declarations.begin();
    while (itd != this->declarations.end())
    {
        Declaration * dec = *itd;
        if(dec != NULL){
            ss<<dec->genCode()<<endl;
        }

        itd++;
    }

    list<Statement *>::iterator its = this->statements.begin();
    while (its != this->statements.end())
    {
        Statement * stmt = *its;
        if(stmt != NULL){
            ss<<stmt->genCode()<<endl;
        }

        its++;
    }
    return ss.str();
}

string ElseIfStatement::genCode(){
    stringstream code;

    if(this->assignExpr != NULL)
    {
        code << this->assignExpr->genCode() << endl;
    }

    string elseLabel = getNewLabel("elseif");
    string endIfLabel = getNewLabel("endElseif");
    Code exprCode;
    
    this->conditionalExpr2->genCode(exprCode);
    code << exprCode.code << endl;
    
    code<< "beqz "<< exprCode.place << ", " << elseLabel <<endl;
        
    code << this->trueStatement->genCode() << endl
    << "j " <<endIfLabel << endl
    << elseLabel <<": " <<endl
    << this->otherStatement->genCode() <<endl
    << endIfLabel <<":"<< endl;
    releaseRegister(exprCode.place);
    return code.str();
}

string ElseStatement::genCode(){ // if -> else if(){}-else{}
    string elseLabel = getNewLabel("else");
    string endIfLabel = getNewLabel("endif");
    Code exprCode;
    stringstream code;
    this->conditionalExpr2->genCode(exprCode);
    code << exprCode.code << endl;
    code<< "beqz "<< exprCode.place << ", " << elseLabel <<endl;
    code << this->trueStatement->genCode() << endl
    << "j " <<endIfLabel << endl
    << elseLabel <<": " <<endl
    << this->falseStatement->genCode() <<endl
    << endIfLabel <<":"<< endl;
    releaseRegister(exprCode.place);
    return code.str();
}

string IfStatement::genCode(){

    stringstream code;
    if(this->assignExpr != NULL)
    {
        code << this->assignExpr->genCode() << endl;
    }

    string endIfLabel = getNewLabel("endif");
    Code exprCode;
    this->conditionalExpr2->genCode(exprCode);

    code << exprCode.code << endl;
    code<< "beqz "<< exprCode.place << ", " << endIfLabel <<endl;
    code<< this->trueStatement->genCode() << endl
    << endIfLabel <<" :"<< endl;
    releaseRegister(exprCode.place);
    
    return code.str();
}

#define IMPLEMENT_BINARY_ARIT_GEN_CODE(name, op)\
void name##Expr::genCode(Code &code, string labelName = ""){\
    Code leftCode, rightCode;\
    stringstream ss;\
    this->expr1->genCode(leftCode);\
    this->expr2->genCode(rightCode);\
    if(leftCode.type == INT && rightCode.type == INT){\
        code.type = INT;\
        ss<< leftCode.code << endl\
        << rightCode.code <<endl\
        << intArithmetic(leftCode, rightCode, code, op)<< endl;\
        releaseRegister(leftCode.place);\
        releaseRegister(rightCode.place);\
    }else{\
        code.type = FLOAT;\
        toFloat(leftCode);\
        toFloat(rightCode);\
        ss << leftCode.code << endl\
        << rightCode.code <<endl\
        << floatArithmetic(leftCode, rightCode, code, op)<<endl;\
        releaseRegister(rightCode.place);\
        releaseRegister(leftCode.place);\
    }\
    code.code = ss.str();\
}\

#define IMPLEMENT_UNARY_ARIT_GEN_CODE(name, op)\
void name##Expr::genCode(Code &code, string labelName = ""){\
    Code leftCode;\
    stringstream ss;\
    this->expr->genCode(leftCode);\
    if(leftCode.type == INT){\
        code.type = INT;\
        ss<< leftCode.code << endl\
        << intArithmetic(leftCode, leftCode, code, op)<< endl;\
        releaseRegister(leftCode.place);\
    }else{\
        code.type = FLOAT;\
        toFloat(leftCode);\
        ss << leftCode.code << endl\
        << floatArithmetic(leftCode, leftCode, code, op)<<endl;\
        releaseRegister(leftCode.place);\
    }\
    code.code = ss.str();\
}\

string intArithmetic(Code &leftCode, Code &rightCode, Code &code, char op){
    stringstream ss;
    code.place = getIntTemp();
    switch (op)
    {
        case '+':
            ss << "add "<< code.place<<", "<< leftCode.place <<", "<< rightCode.place;
            break;
        case '-':
            ss << "sub "<< code.place<<", "<< leftCode.place <<", "<< rightCode.place;
            break;
        case '*':
            ss << "mult "<< leftCode.place <<", "<< rightCode.place <<endl
            << "mflo "<< code.place;
            break;
        case '/':
            ss << "div "<< leftCode.place <<", "<< rightCode.place << endl
            << "mflo "<< code.place;
            break;
        case '^':
        {
            string initialCicle = getNewLabel("powStart");
            string finalCicle = getNewLabel("powEnd");
            string contador = getIntTemp();
            string resultOp = getIntTemp();
            ss<<"li "<<contador<<", 1"<<endl;
            ss<<"move "<< resultOp<< ", "<<leftCode.place<<endl;
            ss<<initialCicle<<":"<<endl;
            ss<<"beq "<<contador<<", "<<rightCode.place<<", "<<finalCicle<<endl;
            ss<<"mult "<<resultOp<<", "<<leftCode.place<<endl;
            ss<<"mflo "<<resultOp<<endl;
            ss<<"addi "<<contador<<", "<<contador<<", 1"<<endl;
            ss<<"j "<<initialCicle<<endl;
            ss<<finalCicle<<":"<<endl;
            ss<<"move "<< code.place<< ", "<<resultOp<<endl;
            releaseIntTemp(contador);
            releaseIntTemp(resultOp);
            break;
        }
        case '%':
        {
            string resultOp = getIntTemp();
            string initialCicle = getNewLabel("modStart");
            string finalCicle = getNewLabel("modEnd");
            ss<< "move "<<resultOp<<", "<< leftCode.place<<endl;
            ss<<initialCicle<<": "<<endl;
            ss<<"bgt "<<rightCode.place<<", "<<resultOp<<", "<<finalCicle<<endl;
            ss<<"sub "<<resultOp<<", "<<resultOp<<", "<<rightCode.place<<endl;
            ss<<"j "<<initialCicle<<endl;
            ss<<finalCicle<<":"<<endl;
            ss<<"move "<< code.place<< ", "<<resultOp<<endl;
            releaseIntTemp(resultOp);
            break;
        }
        default:
            break;
    }
    return ss.str();
}

string floatArithmetic(Code &leftCode, Code &rightCode, Code &code, char op){
    stringstream ss;
    code.place = getFloatTemp();
    switch (op)
    {
        case '+':
            ss << "add.s "<< code.place<<", "<< leftCode.place <<", "<< rightCode.place;
            break;
        case '-':
            ss << "sub.s "<< code.place<<", "<< leftCode.place <<", "<< rightCode.place;
            break;
        case '*':
            ss << "mul.s "<< code.place<<", "<< leftCode.place <<", "<< rightCode.place;
            break;
        case '/':
            ss << "div.s "<< code.place<<", "<< leftCode.place <<", "<< rightCode.place;
            break;
        case '^':
        {
            string initialCicle = getNewLabel("powStart");
            string finalCicle = getNewLabel("powEnd");
            string contador = getFloatTemp();
            string resultOp = getFloatTemp();
            string constante = getFloatTemp();
            ss<<"mov.s "<<resultOp << ", "<<leftCode.place<<endl;
            ss<<"li.s "<<contador<<", 1.0"<<endl;
            ss<<"li.s "<<constante<<", 1.0"<<endl;
            ss<<initialCicle<<":"<<endl;
            ss<<"c.eq.s "<<contador<<", "<<rightCode.place<<endl;
            ss<<"bc1t "<<finalCicle<<endl;
            ss<<"mul.s "<<resultOp<<", "<<resultOp<<", "<<leftCode.place<<endl;
            ss<<"add.s "<<contador<<", "<<contador<<", "<<constante<<endl;
            ss<<"j "<<initialCicle<<endl;
            ss<<finalCicle<<":"<<endl;
            ss<<"mov.s "<< code.place<< ", "<<resultOp<<endl;            
            releaseFloatTemp(contador);
            releaseFloatTemp(resultOp);
            releaseFloatTemp(constante);
            break;
        }
        case '%':
        {
            string resultOp = getFloatTemp();
            string initialCicle = getNewLabel("modStart");
            string finalCicle = getNewLabel("modEnd");
            string continueLabel = getNewLabel("continue");   
            ss<< "mov.s "<<resultOp<<", "<< leftCode.place<<endl;
            ss<<initialCicle<<": "<<endl;
            ss<<"c.le.s "<<rightCode.place<<", "<<resultOp<<endl;
            ss<<"bc1t "<<continueLabel<<endl;
            ss<<"j "<<finalCicle<<endl;
            ss<<continueLabel<<":"<<endl;
            ss<<"sub.s "<<resultOp<<", "<<resultOp<<", "<<rightCode.place<<endl;
            ss<<"j "<<initialCicle<<endl;
            ss<<finalCicle<<":"<<endl;
            ss<<"mov.s "<< code.place<< ", "<<resultOp<<endl;
            releaseFloatTemp(resultOp);
            break;
        }
        default:
            break;
    }
    return ss.str();
}

IMPLEMENT_BINARY_ARIT_GEN_CODE(Add, '+');
IMPLEMENT_BINARY_ARIT_GEN_CODE(Sub, '-');
IMPLEMENT_BINARY_ARIT_GEN_CODE(Mul, '*');
IMPLEMENT_BINARY_ARIT_GEN_CODE(Div, '/');
IMPLEMENT_BINARY_ARIT_GEN_CODE(Pow, '^');
IMPLEMENT_BINARY_ARIT_GEN_CODE(Percent, '%');