%code requires{
     #include "ast.h"
}

%{
    #include <cstdio>
    #include <iostream>
    using namespace std;
    int yylex();
    extern int yylineno;
    void yyerror(const char * s){
        fprintf(stderr, "Line: %d, error: %s\n", yylineno, s);
    }

    #define YYERROR_VERBOSE 1
    #define YYDEBUG 1
    #define EQUAL 1
    #define PLUSEQUAL 2
    #define MINUSEQUAL 3
    #define POTOREQUAL 4
    #define DIVOREQUAL 5
    #define MULTOREQUAL 6
    #define PORCENTOREQUAL 7
    #define COLONOREQUAL 8
    #define OREQUAL 9
    #define ANDEQUAL 10
%}

%union{
    const char * string_t;
    int int_t;
    float float_t;
    Expr * expr_t;
    ArgumentList * argument_list_t;
    ExpressionList* expression_list_t;
    Statement * statement_t;
    StatementList * statement_list_t;
    Parameter * parameter_t;
    ImportList* import_list_t;
    ParameterList * parameter_list_t;
    InitializerElementList * initializer_list_t;
    Initializer * initializer_t;
    InitDeclarator * init_declarator_t;
    Declarator * declarator_t;
    Declaration * declaration_t;
    DeclarationList * declaration_list_t;
    Import* import_t;
    StringValue* string_value_t;
}

%token<string_t>  TK_LIT_STRING TK_ID
%token<int_t>  TK_LIT_INT
%token<float_t>  TK_LIT_FLOAT
%token<int_t> TK_FALSE TK_TRUE


%token EOL
%token TK_PRINTF
%token TK_ADD TK_SUB TK_LESS TK_LEFT_BRACKET TK_RIGHT_BRACKET TK_MUL TK_POW TK_GREATER TK_DIV TK_EQUAL_EQUAL TK_COMMA TK_SEMICOLON TK_PERCENT TK_EXCLAMATION TK_COLON TK_LEFT_KEY TK_RIGHT_KEY TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES

%token TK_INT TK_FLOAT TK_BOOL TK_STRING
%token TK_MINUS_MINUS TK_PORCENT_OR_EQUAL TK_COLON_OR_EQUAL TK_PLUS_PLUS TK_DIV_OR_EQUAL TK_GREATER_OR_EQUAL TK_POT_OR_EQUAL TK_MUL_OR_EQUAL TK_LESS_OR_EQUAL TK_OR TK_OR_EQUAL TK_MINUS_EQUAL TK_NOT_EQUAL TK_EQUAL TK_AND TK_AND_EQUAL TK_PLUS_EQUAL
%token TK_IF TK_ELSE
%token TK_FOR TK_BREAK TK_CONTINUE
%token TK_FUNC TK_RETURN
%token TK_MAIN TK_IMPORT TK_PACKAGE
%token TK_VAR

%type<string_value_t> import
%type<import_t> imports
%type<import_list_t> importsLitString
%type<statement_list_t> statement_list input
%type<declaration_list_t> declaration_list
%type<declaration_t> declaration
%type<declarator_t> declarator
%type<init_declarator_t> init_declarator
%type<initializer_t> initializer
%type<initializer_list_t> initializer_list
%type<parameter_t> parameter_declaration
%type<parameter_list_t> parameters_type_list
%type<expression_list_t> expression_list
%type<argument_list_t> argument_expression_list
%type<int_t> type assignment_operator
%type<expr_t> constant expression logical_and_expression additive_expression multiplicative_expression equality_expression relational_expression pow_expression percent_expression
%type<expr_t> postfix_expression primary_expression boolean
%type<expr_t> assignment_expression logical_or_expression
%type<statement_t> external_declaration method_definition jump_statement for_statement for_expression_statement if_statement block_statement statement expression_statement
%%

start: package eol imports input {
            list<Statement *>::iterator it = $4->begin();
            while(it != $4->end()){
                printf("semantic result: %d \n",(*it)->evaluateSemantic());
                it++;
            }
        }
        | package eol input{
            list<Statement *>::iterator it = $3->begin();
            while(it != $3->end()){
                printf("semantic result: %d \n",(*it)->evaluateSemantic());
                it++;
            }
        }
        ;
        
package: TK_PACKAGE TK_MAIN
        ;

imports: imports import eol  { $$->value.push_back($2);}      
        | import eol { ImportList* list = new ImportList; list->push_back($1);
                        $$ = new Import(*list,yylineno);}
        | TK_IMPORT TK_LEFT_PARENTHESES eol importsLitString TK_RIGHT_PARENTHESES eol {$$ = new Import(*$4,yylineno);}
        ;

importsLitString:  TK_LIT_STRING eol importsLitString 
                    {
                       StringValue* val = new StringValue($1,yylineno);
                        $$ = $3; $$->push_back(val);
                    }
                  | TK_LIT_STRING eol 
                  {
                      StringValue* val = new StringValue($1,yylineno);
                      $$ = new ImportList; $$->push_back(val);
                  }
                ;

import: TK_IMPORT TK_LIT_STRING {$$ = new StringValue($2,yylineno);}
        ;

input: input external_declaration {$$ = $1; $$->push_back($2);}
    | external_declaration {$$ = new StatementList; $$->push_back($1);}
    ;

external_declaration: method_definition eol {$$ = $1;}
            | declaration {$$ = new GlobalDeclaration($1);}
            ;


method_definition: TK_FUNC TK_ID TK_LEFT_PARENTHESES parameters_type_list TK_RIGHT_PARENTHESES type block_statement {
                    $$ = new MethodDefinition((Type)$6, $2, *$4, $7, false, 0,yylineno);
                    delete $4;
                 }
                 | TK_FUNC TK_ID TK_LEFT_PARENTHESES parameters_type_list TK_RIGHT_PARENTHESES TK_LEFT_BRACKET TK_LIT_INT TK_RIGHT_BRACKET type block_statement {
                    $$ = new MethodDefinition((Type)$9, $2, *$4, $10, true, $7,yylineno);
                    delete $4;
                 }
                 | TK_FUNC TK_ID TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES type block_statement {
                     ParameterList * pm = new ParameterList;
                     $$ = new MethodDefinition((Type)$5, $2, *pm, $6, false, 0, yylineno );
                     delete pm;
                 }
                 | TK_FUNC TK_ID TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES TK_LEFT_BRACKET TK_LIT_INT TK_RIGHT_BRACKET type block_statement {
                     ParameterList * pm = new ParameterList;
                     $$ = new MethodDefinition((Type)$8, $2, *pm, $9, true, $6, yylineno );
                     delete pm;
                 }
                 | TK_FUNC TK_ID TK_LEFT_PARENTHESES parameters_type_list TK_RIGHT_PARENTHESES block_statement {
                    $$ = new MethodDefinition(VOID, $2, *$4, $6, false, 0,yylineno);
                    delete $4;
                 }
                 | TK_FUNC TK_ID TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES block_statement {
                     ParameterList * pm = new ParameterList;
                     $$ = new MethodDefinition(VOID, $2, *pm, $5, false, 0, yylineno );
                     delete pm;
                 }
                 | TK_FUNC TK_MAIN TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES block_statement {
                     ParameterList * pm = new ParameterList;
                     $$ = new MethodDefinition(VOID, "main", *pm, $5, false, 0, yylineno );
                     delete pm;
                 }
                ;

declaration_list: declaration_list declaration { $$ = $1; $$->push_back($2); }
                | declaration {$$ = new DeclarationList; $$->push_back($1);}
                ;

declaration: TK_VAR init_declarator eol { $$ = new Declaration($2, yylineno);  }
            ;

init_declarator: declarator type {$$ = new InitDeclarator($1, (Type)$2, NULL, yylineno);}
                | declarator type TK_EQUAL initializer { $$ = new InitDeclarator($1, (Type)$2 ,$4, yylineno); }
                ;
               
declarator: TK_ID  {$$ = new Declarator($1, 0, false, yylineno);}
          | TK_ID TK_LEFT_BRACKET TK_LIT_INT TK_RIGHT_BRACKET { $$ = new Declarator($1, $3, true, yylineno);}
          ;

initializer: assignment_expression {
                                        InitializerElementList * list = new InitializerElementList;
                                        list->push_back($1);
                                        $$ = new Initializer(*list, yylineno);
                                    }
           | TK_LEFT_KEY initializer_list TK_RIGHT_KEY { $$ = new Initializer(*$2, yylineno); delete $2;  }
           ;

parameters_type_list: parameters_type_list TK_COMMA parameter_declaration {$$ = $1; $$->push_back($3);}
                   | parameter_declaration { $$ = new ParameterList; $$->push_back($1); }
                   ;
                   
parameter_declaration: declarator type { $$ = new Parameter((Type)$2,$1, false, yylineno); }
                    ;

initializer_list: initializer_list TK_COMMA logical_or_expression { $$ = $1; $$->push_back($3); }
                | logical_or_expression  {$$ = new InitializerElementList; $$->push_back($1);}
                ;

statement: expression_statement {$$ = $1;}
        | if_statement eol {$$ = $1;}
        | for_statement eol {$$ = $1;}
        | block_statement eol {$$ = $1;}
        | jump_statement {$$ = $1;}
        | TK_PRINTF TK_LEFT_PARENTHESES expression_list TK_RIGHT_PARENTHESES eol {$$ = new PrintStatement(*$3, yylineno);}
        ;

expression_list: expression_list TK_COMMA expression {$$ = $1;$$->push_back($3);}
               | expression {$$ = new ExpressionList;$$->push_back($1);}
               ;


statement_list: statement_list statement { $$ = $1; $$->push_back($2); }
              | statement { $$ = new StatementList; $$->push_back($1); }
              ;


block_statement: TK_LEFT_KEY eol statement_list TK_RIGHT_KEY { 
                    DeclarationList * decls = new DeclarationList();
                    $$ = new BlockStatement(*$3, *decls, yylineno);
                    delete decls;
               }
               | TK_LEFT_KEY eol declaration_list TK_RIGHT_KEY { 
                   StatementList * stmts = new StatementList();
                    $$ = new BlockStatement(*stmts, *$3, yylineno);
                    delete stmts;
               }
               | TK_LEFT_KEY eol declaration_list statement_list TK_RIGHT_KEY {$$ = new BlockStatement(*$4, *$3, yylineno); delete $4; delete $3; }
               | TK_LEFT_KEY eol statement_list declaration_list TK_RIGHT_KEY {$$ = new BlockStatement(*$3, *$4, yylineno); delete $4; delete $3; }
               | TK_LEFT_KEY eol TK_RIGHT_KEY  {
                   StatementList * stmts = new StatementList();
                   DeclarationList * decls = new DeclarationList();
                   $$ = new BlockStatement(*stmts, *decls, yylineno);
                   delete stmts;
                   delete decls;

               }
               ;

if_statement: TK_IF  expression block_statement {$$ = new IfStatement($2,NULL, $3, yylineno);}
            | TK_IF  expression block_statement TK_ELSE block_statement {$$ = new ElseStatement($2,NULL, $3,$5, yylineno);}
            | TK_IF  expression block_statement TK_ELSE if_statement {$$ = new ElseIfStatement($2,NULL, $3,$5, yylineno);}
            | TK_IF  for_expression_statement expression  block_statement {$$ = new IfStatement($3, $2,$4, yylineno);}
            | TK_IF  for_expression_statement expression block_statement TK_ELSE block_statement {$$ = new ElseStatement($3, $2,$4,$6, yylineno);}
            | TK_IF  for_expression_statement expression block_statement TK_ELSE if_statement {$$ = new ElseIfStatement($3,$2, $4, $6, yylineno);}
            ;

for_statement: TK_FOR for_expression_statement for_expression_statement expression block_statement {$$ = new ForStatement($2,$3,$4,$5,yylineno);}
             | TK_FOR expression block_statement {$$ = new ForStatement(NULL,NULL,$2,$3,yylineno);}
             | TK_FOR block_statement {$$ = new ForStatement(NULL,NULL,NULL,$2,yylineno);}
            ;

for_expression_statement: expression TK_SEMICOLON {$$ = new ExprStatement($1, yylineno);}
                        ;

expression_statement: expression eol {$$ = new ExprStatement($1, yylineno);}
                    ;

jump_statement: TK_RETURN eol {$$ = new ReturnStatement(NULL, yylineno);}
              | TK_CONTINUE eol {$$ = new ContinueStatement(yylineno);}
              | TK_BREAK eol {$$ = new BreakStatement(yylineno);}
              | TK_RETURN expression eol {$$ = new ReturnStatement($2, yylineno);}
              ;

type: TK_INT {$$ = INT;}
    | TK_FLOAT {$$ = FLOAT;}
    | TK_BOOL {$$ = BOOL;}
    | TK_STRING {$$ = STRING;}
    ;

primary_expression: TK_ID {$$ = new IdExpr($1, yylineno);}
    | constant {$$ = $1;}
    | TK_LEFT_PARENTHESES expression TK_RIGHT_PARENTHESES {$$ = $2;}
    ;

 
assignment_expression: postfix_expression assignment_operator logical_or_expression {$$ = new AssignExpr($1,$3,$2,yylineno);}
                     | logical_or_expression {$$ = $1;}
                     ;

postfix_expression: primary_expression {$$ = $1;}
                    | postfix_expression TK_LEFT_BRACKET expression TK_RIGHT_BRACKET { $$ = new ArrayExpr((IdExpr*)$1, $3, yylineno); }
                    | postfix_expression TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES { $$ = new MethodInvocationExpr((IdExpr*)$1, *(new ArgumentList), yylineno); }
                    | postfix_expression TK_LEFT_PARENTHESES argument_expression_list TK_RIGHT_PARENTHESES { $$ = new MethodInvocationExpr((IdExpr*)$1, *$3, yylineno); }
                    | postfix_expression TK_PLUS_PLUS { $$ = new PostIncrementExpr((IdExpr*)$1, yylineno);}
                    | postfix_expression TK_MINUS_MINUS { $$ = new PostDecrementExpr((IdExpr*)$1, yylineno); }
                    | TK_LEFT_BRACKET TK_LIT_INT TK_RIGHT_BRACKET type TK_LEFT_KEY argument_expression_list TK_RIGHT_KEY {$$ = new ArrayInitExpr($2,$4,*$6,yylineno);}
                    ;


argument_expression_list: argument_expression_list TK_COMMA assignment_expression {$$ = $1;  $$->push_back($3);}
                        | assignment_expression { $$ = new ArgumentList; $$->push_back($1);}
                        ;
              
percent_expression: percent_expression TK_PERCENT postfix_expression { $$ = new PercentExpr($1, $3, yylineno); }
                  | postfix_expression {$$ = $1;}
                  ;

pow_expression: pow_expression TK_POW percent_expression { $$ = new PowExpr($1, $3, yylineno); }
              | percent_expression {$$ = $1;}
              ;

multiplicative_expression: multiplicative_expression TK_MUL pow_expression { $$ = new MulExpr($1, $3, yylineno); }
      | multiplicative_expression TK_DIV pow_expression { $$ = new DivExpr($1, $3, yylineno); }
      | pow_expression {$$ = $1;}
      ;
                  
additive_expression:  additive_expression TK_ADD multiplicative_expression { $$ = new AddExpr($1, $3, yylineno); }
                    | additive_expression TK_SUB multiplicative_expression { $$ = new SubExpr($1, $3, yylineno); }
                    | multiplicative_expression {$$ = $1;}
                    ;

relational_expression: relational_expression TK_GREATER additive_expression { $$ = new GtExpr($1, $3, yylineno); }
                     | relational_expression TK_LESS additive_expression { $$ = new LtExpr($1, $3, yylineno); }
                     | relational_expression TK_GREATER_OR_EQUAL additive_expression { $$ = new GteExpr($1, $3, yylineno); }
                     | relational_expression TK_LESS_OR_EQUAL additive_expression { $$ = new LteExpr($1, $3, yylineno); }
                     | additive_expression {$$ = $1;}
                     ;

equality_expression:  equality_expression TK_EQUAL_EQUAL relational_expression { $$ = new EqExpr($1, $3, yylineno); }
                   | equality_expression TK_NOT_EQUAL relational_expression { $$ = new NeqExpr($1, $3, yylineno); }
                   | relational_expression {$$ = $1;}
                   ;

logical_or_expression: logical_or_expression TK_OR logical_and_expression { $$ = new LogicalOrExpr($1, $3, yylineno); }
                    | logical_and_expression {$$ = $1;}
                    ;

logical_and_expression: logical_and_expression TK_AND equality_expression { $$ = new LogicalAndExpr($1, $3, yylineno); }
                      | equality_expression {$$ = $1;}
                      ;


assignment_operator: TK_COLON_OR_EQUAL { $$ = COLONOREQUAL; }//declaramos variable y guardamos
                   | TK_EQUAL { $$ = EQUAL; }
                   | TK_PLUS_EQUAL {$$ = PLUSEQUAL; }
                   | TK_MINUS_EQUAL { $$ = MINUSEQUAL; }
                   | TK_POT_OR_EQUAL { $$ = POTOREQUAL; }
                   | TK_DIV_OR_EQUAL { $$ = DIVOREQUAL; }
                   | TK_MUL_OR_EQUAL { $$ = MULTOREQUAL; }
                   | TK_PORCENT_OR_EQUAL { $$ = PORCENTOREQUAL; }
                   | TK_OR_EQUAL { $$ = OREQUAL; }
                   | TK_AND_EQUAL { $$ = ANDEQUAL; }
                   ;

expression: assignment_expression {$$ = $1;}
          ;

constant: TK_LIT_INT { $$ = new IntExpr($1 , yylineno);}
        | TK_LIT_FLOAT { $$ = new FloatExpr($1 , yylineno);}
        | TK_LIT_STRING { $$ = new StringExpr($1 , yylineno);}
        | boolean {$$ = $1;}
        ;

boolean: TK_TRUE {$$ = new BoolExpr($1 , yylineno);}
        | TK_FALSE {$$ = new BoolExpr($1 , yylineno);}
        ;
        
eol: EOL eol
    | EOL 
    ;
%%



