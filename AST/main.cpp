#include <stdio.h>
#include <string>

using namespace std;

extern FILE * yyin;
extern char* yytext;
extern int yylineno;

int yylex(void);
int yyparse();

int main(int argc, char* argv[]){
    if(argc != 2){
        fprintf(stderr, "Hace falta el archivo de entrada %s\n", argv[0]);
        return 1;
    }
    
    FILE * f = fopen(argv[1], "r");
    if (f == NULL)
    {
        fprintf(stderr, "No se pudo abrir el archivo %s\n", argv[1]);
        return 1;
    }

    yyin = f;
/*
    int token = yylex();
    while(token != 0){
        printf("Linea:%d - Token: %d - Lexema: %s\n",yylineno, token, yytext);
        token = yylex();
    }*/

     yyparse();



    return 0;
}