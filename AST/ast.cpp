#include "ast.h"
#include <iostream>

Type EnumOfIndex(int i) 
{
     return static_cast<Type>(i); 
}

string getTypeName(Type type){
    switch(type){
        case INT:
            return "INT";
        case FLOAT:
            return "FLOAT";
        case INT_ARRAY:
            return "INT";
        case FLOAT_ARRAY:
            return "FLOAT";
        case BOOL:
            return "BOOL";
        case VOID:
            return "VOID";
        case STRING:
            return "STRING";
    }

    cout<<"Unknown type"<<endl;
    exit(0);
}


class ContextStack{
    public:
        struct ContextStack* prev;
        map<string, Type> variables;
        string methodParent;
        bool inFor;
};

class FunctionInfo{
    public:
        Type returnType;
        list<Parameter *> parameters;
};

map<string, Type> globalVariables = {};
map<string, Type> variables;
map<string, FunctionInfo*> methods;


void addMethodDeclaration(string id, int line, Type type, ParameterList params){
    if(methods[id] != 0){
        cout<<"redefinition of function "<<id<<" in line: "<<line<<endl;
        exit(0);
    }
    methods[id] = new FunctionInfo();
    methods[id]->returnType = type;
    methods[id]->parameters = params;
}

ContextStack * context = NULL;

void pushContext(string methodName, bool inFor){
    variables.clear();
    ContextStack * c = new ContextStack();
    c->variables = variables;
    c->prev = context;
    if (c->prev == NULL)
    {
        c->inFor = inFor;
        c->methodParent = methodName;
    }
    else
    {
        c->methodParent = c->prev->methodParent;
        if(c->prev->inFor)
        {
            c->inFor = true;
        }
        else
        {
            c->inFor = inFor;
        }
    }   
    
    context = c;
}

void popContext(){
    if(context != NULL){
        ContextStack * previous = context->prev;
        free(context);
        context = previous;
    }
}


Type getLocalVariableType(string id){
    ContextStack * currContext = context;
    while(currContext != NULL){
        if(currContext->variables[id] != 0)
            return currContext->variables[id];
        currContext = currContext->prev;
    }
    if(!context->variables.empty())
        return context->variables[id];
    return INVALID;
}


Type getVariableType(string id){
    if(!globalVariables.empty())
        return globalVariables[id];
    return INVALID;
}


bool variableExists(string id){
  Type value;
  if(context != NULL){
    value = getLocalVariableType(id);
    if(value != 0)
      return true;
  }
  return false;
}


Type MethodInvocationExpr::getType(){
    FunctionInfo * func = methods[this->id->id];
    if(func == NULL){
        cout<<"error: function "<<this->id->id<<" not found, line: "<<this->line<<endl;
        exit(0);
    }
    Type funcType = func->returnType;
    if(func->parameters.size() > this->args.size()){
        cout<<"error: to few arguments to function"<<this->id->id<<" line: "<<this->line<<endl;
        exit(0);
    }
    if(func->parameters.size() < this->args.size()){
        cout<<"error: to many arguments to function "<<this->id->id<<" line: "<<this->line<<endl;
        exit(0);
    }

    list<Parameter *>::iterator paramIt = func->parameters.begin();
    list<Expr *>::iterator argsIt =this->args.begin();
    while(paramIt != func->parameters.end() && argsIt != this->args.end()){
        string paramType = getTypeName((*paramIt)->type);
        string argType = getTypeName((*argsIt)->getType());
        if( paramType != argType){
            cout<<"error: invalid conversion from: "<< argType <<" to " <<paramType<< " line: "<<this->line <<endl;
            exit(0);
        }
        paramIt++;
        argsIt++;
    }

    return funcType;
}

map<string, Type> resultTypes ={
    {"INT,INT", INT},
    {"FLOAT,FLOAT", FLOAT},
    {"INT,FLOAT", FLOAT},
    {"FLOAT,INT", FLOAT},
    {"BOOL,BOOL", BOOL},
    {"STRING,STRING", STRING},
};

bool canConvert(Type t1,Type t2)
{
    if (t1 == FLOAT)
    {
        if (t2 == INT || t2 == FLOAT)
        {
            return true;
        }
        
        return false;        
    }
    
    return t1 == t2;
}



Type IntExpr::getType(){
    return INT;
}

Type BoolExpr::getType(){
    return BOOL;
}

Type FloatExpr::getType(){
    return FLOAT;
}

Type PostIncrementExpr::getType(){
    return this->expr->getType();
}

Type PostDecrementExpr::getType(){
    return this->expr->getType();
}

Type ArrayInitExpr::getType(){
    return EnumOfIndex(type);
}

Type ArrayExpr::getType(){
    return this->id->getType();
}

Type StringExpr::getType(){
    return STRING;
}






Type IdExpr::getType(){
    Type value;
    if(context != NULL){
        value = getLocalVariableType(this->id);
        if(value != 0)
            return value;
    }
    value = getVariableType(this->id);
    if(value == 0){
        cout<<"error: '"<<this->id<<"' was not declared in this scope. line: "<<this->line<<endl;
        exit(0);
    }
    return value;
}



int MethodDefinition::evaluateSemantic(){
    if(this->params.size() > 4){
        cout<< "Method: "<<this->id << " can't have more than 4 parameters, line: "<< this->line<<endl;
        exit(0);
    }

    addMethodDeclaration(this->id, this->line, this->type, this->params);
    pushContext(this->id,false);
   
    list<Parameter* >::iterator it = this->params.begin();
    while(it != this->params.end()){
        (*it)->evaluateSemantic();
        it++;
    }

    if(this->statement !=NULL ){
        this->statement->evaluateSemantic();
    }
    
    popContext();

    return 0;
}


int IfStatement::evaluateSemantic(){

    
    if (this->assignExpr != NULL)
    {
        this->assignExpr->evaluateSemantic();
    }

    if(this->conditionalExpr2 != NULL && this->conditionalExpr2->getType() != BOOL){
        cout<<"Expression for if must be boolean"<<endl;
        exit(0);
    }

    pushContext("",false);
    this->trueStatement->evaluateSemantic();
    popContext();
    return 0;
}


int ElseStatement::evaluateSemantic(){
    
    if (this->assignExpr != NULL)
    {
        this->assignExpr->evaluateSemantic();
    }
    
    if(this->conditionalExpr2 != NULL && this->conditionalExpr2->getType() != BOOL){
        cout<<"Expression for if must be boolean"<<endl;
        exit(0);
    }
    pushContext("",false);
    this->trueStatement->evaluateSemantic();
    popContext();
    pushContext("",false);
    if(this->falseStatement != NULL)
        this->falseStatement->evaluateSemantic();
    popContext();
    return 0;
}

int ElseIfStatement::evaluateSemantic()
{
   
    if (this->assignExpr != NULL)
    {
        this->assignExpr->evaluateSemantic();
    }
    
    if(this->conditionalExpr2 != NULL && this->conditionalExpr2->getType() != BOOL){
        cout<<"Expression for if must be boolean"<<endl;
        exit(0);
    }
    pushContext("",false);
    this->trueStatement->evaluateSemantic();
    popContext();
    pushContext("",false);
    if(this->otherStatement != NULL)
        this->otherStatement->evaluateSemantic();
    popContext();
    return 0;

}

int Parameter::evaluateSemantic(){
    if(!variableExists(this->declarator->id)){
        context->variables[declarator->id] = this->type;
    }else{
        cout<<"error: redefinition of variable: "<< declarator->id<< " line: "<<this->line <<endl;
        exit(0);
    }
    return 0;
}

int BlockStatement::evaluateSemantic(){
    list<Declaration *>::iterator itd = this->declarations.begin();
    while (itd != this->declarations.end())
    {
        Declaration * dec = *itd;
        if(dec != NULL){
            dec->evaluateSemantic();
        }

        itd++;
    }

    list<Statement *>::iterator its = this->statements.begin();
    while (its != this->statements.end())
    {
        Statement * stmt = *its;
        if(stmt != NULL){
            stmt->evaluateSemantic();
        }

        its++;
    }

    return 0;
}

int ForStatement::evaluateSemantic()
{
    if (this->assignExpr != NULL)
    {
        this->assignExpr->evaluateSemantic();
    }
    
    if (this->conditionalExpr != NULL)
    {        
        ExprStatement* conditional = dynamic_cast<ExprStatement*>(conditionalExpr);

        if (conditional->expr->getType() != BOOL)
        {
            cout<<"Expression for FOR must be boolean"<<endl;
            exit(0);
        }
        
        this->conditionalExpr->evaluateSemantic();        
    }
    
    if (this->statement != NULL)    
    {
        pushContext("",true);
        this->statement->evaluateSemantic();
        popContext();
    }
    
    return 0;
}

int PrintStatement::evaluateSemantic()
{
    return this->exprs.front()->getType();
}

int ReturnStatement::evaluateSemantic()
{
    string methodName = context->methodParent;
    FunctionInfo* f = methods[methodName];
    
    bool result = canConvert(f->returnType, this->expr->getType());
    if (!result)
    {
        cout<<"error: invalid conversion from: "<< getTypeName(this->expr->getType()) <<" to " <<getTypeName(f->returnType)<< " line: "<<this->line <<endl;
        exit(0);
    }

    return 0;
}

int ContinueStatement::evaluateSemantic()
{
    if(!context->inFor)
    {
        cout<<"error: Invalid instruction. line: "<<this->line <<endl;
        exit(0);
    }

    //for
    return 0;
}

int ExprStatement::evaluateSemantic()
{
    
    this->expr->evaluateSemantic();
    return 0;
}

int BreakStatement::evaluateSemantic()
{
    if(!context->inFor)
    {
        cout<<"error: Invalid instruction. line: "<<this->line <<endl;
        exit(0);
    }

    return 0;
}

int GlobalDeclaration::evaluateSemantic()
{
    if (this->declaration == NULL)
    {
        cout<<"Declaration can't be NULL."<<endl;
        exit(0);
    }

    if(this->declaration->initDeclarator == NULL)
    {
        cout<<"Init declarator can't be NULL."<<endl;
        exit(0);
    }

    Type originalType = this->declaration->initDeclarator->type; 
    string id = this->declaration->initDeclarator->declarator->id;

    if(globalVariables[id] != 0)
    {
        cout<<"error: redefinition of variable: "<< id << " line: "<<this->line <<endl;
        exit(0);
    }

    
    if (this->declaration->initDeclarator->declarator->isArray)
    {
        int size = this->declaration->initDeclarator->declarator->sizeArray;
        if (size <= 0)
        {
            cout<<"error: array size is not valid. line: "<<this->line <<endl;
            exit(0);
        }
        
        if(this->declaration->initDeclarator->initializer == NULL)
        {
            globalVariables[id] = originalType;
        }
        else
        {
            list<Expr *>::iterator ite = this->declaration->initDeclarator->initializer->expressions.begin();
            int size2 = this->declaration->initDeclarator->initializer->expressions.size();
            if (size<size2)
            {
                cout<<"error: index out of range. line: "<<this->line <<endl;
                exit(0);
            }
            
            while(ite!=  this->declaration->initDeclarator->initializer->expressions.end())
            {
                Type exprType = (*ite)->getType();
                bool result = canConvert(originalType,exprType);
                if(!result)
                {
                    cout<<"error: invalid conversion from: "<< getTypeName(exprType) <<" to " <<getTypeName(originalType)<< " line: "<<this->line <<endl;
                    exit(0);
                }
                ite++;
            }

             globalVariables[id] = originalType;
        }
    }
    else
    {
        if(this->declaration->initDeclarator->initializer == NULL)
        {
            globalVariables[id] = originalType;
        }
        else
        {
            int sizeExpr = this->declaration->initDeclarator->initializer->expressions.size();
            if(sizeExpr != 1)
            {
                cout<<"error: too many expressions. Line:"<<this->line <<endl;
                exit(0);
            }
            else
            {
                Type exprType = this->declaration->initDeclarator->initializer->expressions.front()->getType();
                bool result = canConvert(originalType,exprType);
                if(!result)
                {
                    cout<<"error: invalid conversion from: "<< getTypeName(exprType) <<" to " <<getTypeName(originalType)<< " line: "<<this->line <<endl;
                    exit(0);
                }
                globalVariables[id] = originalType;
            }
        }
    }
    return 0;
}

int AssignExpr::evaluateSemantic()
{
    if (this->type == 8)
    {
        if(IdExpr* d = dynamic_cast<IdExpr*>(expr1))
        {
            if (!variableExists(d->id))
            {
                context->variables[d->id] = expr2->getType();
            }
            else
            {
                cout<<"error: redefinition of variable: "<< d->id << " line: "<<this->line <<endl;
                exit(0);
            }
        }
    }

    
    string leftType = getTypeName(this->expr1->getType());
    string rightType = getTypeName(this->expr2->getType());
    Type resultType = resultTypes[leftType+","+rightType];
    if(resultType == 0){
        cerr<< "Error: type "<< leftType <<" can't be converted to type "<< rightType <<" line: "<<this->line<<endl;
        exit(0);
    }
    this->expr2->evaluateSemantic();
    return 0;
}

int Declaration::evaluateSemantic()
{
    if (this->initDeclarator == NULL)
    {
        cout<<"Init declarator can't be NULL."<<endl;
        exit(0);
    }

    Type originalType = this->initDeclarator->type; 
    string id = this->initDeclarator->declarator->id;

    if(variableExists(id))
    {
        cout<<"error: redefinition of variable: "<< id << " line: "<<this->line <<endl;
        exit(0);
    }

    if (this->initDeclarator->declarator->isArray)
    {
        int size = this->initDeclarator->declarator->sizeArray;
        if (size <= 0)
        {
            cout<<"error: array size is not valid. line: "<<this->line <<endl;
            exit(0);
        }
        
        if(this->initDeclarator->initializer == NULL)
        {
            context->variables[id] = originalType;
        }
        else
        {
            list<Expr *>::iterator ite = this->initDeclarator->initializer->expressions.begin();
            int size2 = this->initDeclarator->initializer->expressions.size();
            if (size<size2)
            {
                cout<<"error: index out of range. line: "<<this->line <<endl;
                exit(0);
            }
            
            while(ite!=  this->initDeclarator->initializer->expressions.end())
            {
                Type exprType = (*ite)->getType();
                bool result = canConvert(originalType,exprType);
                if(!result)
                {
                    cout<<"error: invalid conversion from: "<< getTypeName(exprType) <<" to " <<getTypeName(originalType)<< " line: "<<this->line <<endl;
                    exit(0);
                }
                ite++;
            }

             context->variables[id] = originalType;
        }
    }
    else
    {
        if(this->initDeclarator->initializer == NULL)
        {
            context->variables[id] = originalType;
        }
        else
        {
            int sizeExpr = this->initDeclarator->initializer->expressions.size();
            if(sizeExpr != 1)
            {
                cout<<"error: too many expressions. Line:"<<this->line <<endl;
                exit(0);
            }
            else
            {
                Type exprType = this->initDeclarator->initializer->expressions.front()->getType();
                bool result = canConvert(originalType,exprType);
                if(!result)
                {
                    cout<<"error: invalid conversion from: "<< getTypeName(exprType) <<" to " <<getTypeName(originalType)<< " line: "<<this->line <<endl;
                    exit(0);
                }
                context->variables[id] = originalType;
            }
        }
    }
    return 0;
}


int ArrayExpr::evaluateSemantic(){
    return 0;
}

int MethodInvocationExpr::evaluateSemantic(){
      FunctionInfo * func = methods[this->id->id];
    if(func == NULL){
        cout<<"error: function "<<this->id->id<<" not found, line: "<<this->line<<endl;
        exit(0);
    }
    Type funcType = func->returnType;
    if(func->parameters.size() > this->args.size()){
        cout<<"error: to few arguments to function"<<this->id->id<<" line: "<<this->line<<endl;
        exit(0);
    }
    if(func->parameters.size() < this->args.size()){
        cout<<"error: to many arguments to function "<<this->id->id<<" line: "<<this->line<<endl;
        exit(0);
    }

    list<Parameter *>::iterator paramIt = func->parameters.begin();
    list<Expr *>::iterator argsIt =this->args.begin();
    while(paramIt != func->parameters.end() && argsIt != this->args.end()){
        string paramType = getTypeName((*paramIt)->type);
        string argType = getTypeName((*argsIt)->getType());
        if( paramType != argType){
            cout<<"error: invalid conversion from: "<< argType <<" to " <<paramType<< " line: "<<this->line <<endl;
            exit(0);
        }
        paramIt++;
        argsIt++;
    }

    return 0;
}

int ArrayInitExpr::evaluateSemantic(){
    if(this->size < this->args.size()){
        cout<<"error: to many arguments, in array size: "<<this->size<<" line: "<<this->line<<endl;
        exit(0);
    }
    Type arrayType = EnumOfIndex(this->type); //Castear el int a tipo TYPE
    list<Expr *>::iterator argsIt =this->args.begin();
     while(argsIt != this->args.end()){
         
        Type valType = (*argsIt)->getType();
        if(arrayType != valType ){
            if(!canConvert(arrayType, valType))
            {
                cout<<"error: invalid value type: "<< getTypeName(valType) <<" in array type" <<getTypeName(arrayType)<< " line: "<<this->line <<endl;
                exit(0);
            }
        }
        argsIt++; 
    }
    return 0;
}

#define IMPLEMENT_BINARY_EVALUATE_SEMANTIC(name)\
int name##Expr::evaluateSemantic(){\
        return this->getType(); \
}\


#define IMPLEMENT_BINARY_GET_TYPE(name)\
Type name##Expr::getType(){\
    string leftType = getTypeName(this->expr1->getType());\
    string rightType = getTypeName(this->expr2->getType());\
    Type resultType = resultTypes[leftType+","+rightType];\
    if(resultType == 0){\
        cerr<< "Error: type "<< leftType <<" can't be converted to type "<< rightType <<" line: "<<this->line<<endl;\
        exit(0);\
    }\
    return resultType; \
}\

#define IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(name)\
Type name##Expr::getType(){\
    string leftType = getTypeName(this->expr1->getType());\
    string rightType = getTypeName(this->expr2->getType());\
    Type resultType = resultTypes[leftType+","+rightType];\
    if(resultType == 0){\
        cerr<< "Error: type "<< leftType <<" can't be converted to type "<< rightType <<" line: "<<this->line<<endl;\
        exit(0);\
    }\
    return BOOL; \
}\

IMPLEMENT_BINARY_GET_TYPE(Add);
IMPLEMENT_BINARY_GET_TYPE(Sub);
IMPLEMENT_BINARY_GET_TYPE(Mul);
IMPLEMENT_BINARY_GET_TYPE(Div);
IMPLEMENT_BINARY_GET_TYPE(Assign);
IMPLEMENT_BINARY_GET_TYPE(Percent);

IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(Eq);
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(Neq);
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(Gte); 
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(Lte);
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(Gt);
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(Lt);
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(LogicalAnd);
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(LogicalOr);
IMPLEMENT_BINARY_BOOLEAN_GET_TYPE(Pow);

IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Add);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Sub);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Mul);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Div);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Eq);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Neq);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Gte);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Lte);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Gt);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Lt);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(LogicalAnd);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(LogicalOr);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Percent);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Pow);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Binary);


IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Int);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Bool);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Float);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(String);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(PostIncrement);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(PostDecrement);
IMPLEMENT_BINARY_EVALUATE_SEMANTIC(Id);