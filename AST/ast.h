#include <string>
#include <list>
#include <map>

using namespace std;

class Import;
class StringValue;
class Expr;
class Parameter;
class Statement;
class Declaration;
class InitDeclarator;
class Initializer;
class Declarator;
class GlobalDeclaration;
typedef list<Expr *> ArgumentList;
typedef list<Expr *> ExpressionList;
typedef list<Statement *> StatementList;
typedef list<Declaration *> DeclarationList;
typedef list<InitDeclarator *> InitDeclaratorList;
typedef list<Expr *> InitializerElementList;
typedef list<Parameter *> ParameterList;
typedef list<StringValue* > ImportList;

enum StatementKind{
    WHILE_STATEMENT,
    FOR_STATEMENT,
    IF_STATEMENT,
    EXPRESSION_STATEMENT,
    RETURN_STATEMENT,
    CONTINUE_STATEMENT,
    BREAK_STATEMENT,
    BLOCK_STATEMENT,
    PRINT_STATEMENT,
    FUNCTION_DEFINITION_STATEMENT,
    GLOBAL_DECLARATION_STATEMENT,
    ELSE_STATEMENT,
    ELSE_IF_STATEMENT
};



enum Type{
    INVALID,
    STRING,
    INT,
    FLOAT,
    INT_ARRAY,
    FLOAT_ARRAY,
    BOOL,
    VOID
};



class Statement{
    public:
        int line;
        virtual int evaluateSemantic() = 0;
        virtual StatementKind getKind() = 0;
};

class GlobalDeclaration : public Statement {
    public:
        GlobalDeclaration(Declaration * declaration){
            this->declaration = declaration;
        }
        Declaration * declaration;
        int evaluateSemantic();
        StatementKind getKind(){
            return GLOBAL_DECLARATION_STATEMENT;
        }
};

class MethodDefinition : public Statement{
    public:
        MethodDefinition(Type type, string id, ParameterList params, Statement * statement, bool isArray, int sizeAray, int line){
            this->type = type;
            this->id = id;
            this->params = params;
            this->statement = statement;
            this->line = line;
            this->isArray = isArray;
            this->sizeAray = sizeAray;
        }

        bool isArray;
        int sizeAray;
        Type type;
        string id;
        ParameterList params;
        Statement * statement;
        int line;
        int evaluateSemantic();
        StatementKind getKind(){
            return FUNCTION_DEFINITION_STATEMENT;
        }
};

class PrintStatement : public Statement{
    public:
        PrintStatement(ExpressionList exprs, int line){
            this->exprs = exprs;
            this->line = line;
        }
        ExpressionList exprs;
        int evaluateSemantic();
        StatementKind getKind(){return PRINT_STATEMENT;}
};

class BlockStatement : public Statement{
    public:
        BlockStatement(StatementList statements, DeclarationList declarations, int line){
            this->statements = statements;
            this->declarations = declarations;
            this->line = line;
        }
        StatementList statements;
        DeclarationList declarations;
        int line;
        int evaluateSemantic();
        StatementKind getKind(){
            return BLOCK_STATEMENT;
        }
};


class Import{
    public:
        Import(ImportList value,int line){
            this->line = line;
            this->value = value;
        }
        int line;
        ImportList value;
        int evaluateSemantic();
};

class ReturnStatement : public Statement{
    public:
        ReturnStatement(Expr * expr, int line){
            this->expr = expr;
            this->line = line;
        }
        Expr * expr;
        int evaluateSemantic();
        StatementKind getKind(){return RETURN_STATEMENT;}
};

class IfStatement : public Statement{
    public:
        IfStatement(Expr * conditionalExpr2,Statement * assignExpr, Statement * trueStatement, int line){
            this->assignExpr = assignExpr;
            this->conditionalExpr2 = conditionalExpr2;
            this->trueStatement = trueStatement;
            this->line = line;
        }
        Expr * conditionalExpr2;
        Statement * assignExpr;
        Statement * trueStatement;
        int evaluateSemantic();
        StatementKind getKind(){return IF_STATEMENT;}
};



class ElseStatement : public Statement{
    public:
        ElseStatement(Expr * conditionalExpr2,Statement * assignExpr, Statement * trueStatement, Statement * falseStatement, int line){
            this->assignExpr = assignExpr;
            this->conditionalExpr2 = conditionalExpr2;
            this->trueStatement = trueStatement;
            this->line = line;
            this->falseStatement = falseStatement;
        }
        Statement * assignExpr;
        Expr * conditionalExpr2;
        Statement * trueStatement;
        Statement * falseStatement;
        int evaluateSemantic();
        StatementKind getKind(){return ELSE_STATEMENT;}
};

class ElseIfStatement : public Statement{
    public:
        ElseIfStatement(Expr * conditionalExpr2,Statement * assignExpr, Statement * trueStatement, Statement * otherStatement, int line){
            this->assignExpr = assignExpr;
            this->conditionalExpr2 = conditionalExpr2;
            this->trueStatement = trueStatement;
            this->line = line;
            this->otherStatement = otherStatement;
        }
        Statement * assignExpr;
        Expr * conditionalExpr2;
        Statement * trueStatement;
        Statement * otherStatement;
        int evaluateSemantic();
        StatementKind getKind(){return ELSE_IF_STATEMENT;}
};




class ForStatement : public Statement{
    public:
        ForStatement(Statement * assignExpr,Statement * conditionalExpr, Expr* expr, Statement * statement, int line){
            this->assignExpr = assignExpr;
            this->conditionalExpr = conditionalExpr;
            this->expr = expr;
            this->statement = statement;
            this->line = line;
        }
        int line;
        Expr * expr;
        Statement * assignExpr;
        Statement * conditionalExpr;
        Statement * statement;
        int evaluateSemantic();
        StatementKind getKind(){return FOR_STATEMENT;}
};


class ContinueStatement : public Statement{
    public:
        ContinueStatement(int line){
            this->line = line;
        }
        int line;
        int evaluateSemantic();
        StatementKind getKind(){return CONTINUE_STATEMENT;}
};

class BreakStatement : public Statement{
    public:
        BreakStatement(int line){
            this->line = line;
        }
        int line;
        int evaluateSemantic();
        StatementKind getKind(){return BREAK_STATEMENT;}
};

class ExprStatement : public Statement{
    public:
        ExprStatement(Expr * expr, int line){
            this->expr = expr;
            this->line = line;
        }
        Expr * expr;
        int evaluateSemantic();
        StatementKind getKind(){return EXPRESSION_STATEMENT;}
};

class Initializer{
    public:
        Initializer(InitializerElementList expressions, int line){
            this->expressions = expressions;
            this->line = line;
        }
        InitializerElementList expressions;
        int line;
};

class InitDeclarator{
    public:
        InitDeclarator(Declarator * declarator, Type type,Initializer * initializer, int line){
            this->declarator = declarator;
            this->initializer = initializer;
            this->line = line;
            this->type = type;
        }
        Type type;
        Declarator * declarator;
        Initializer * initializer;
        int line;
};

class Declaration{
    public:
        Declaration(InitDeclarator* initDeclarator, int line){
            this->initDeclarator = initDeclarator;
            this->line = line;
        }
        InitDeclarator* initDeclarator;
        int line;
        int evaluateSemantic();
};

class Declarator{
    public:
        Declarator(string id, int sizeArray, bool isArray, int line){
            this->id = id;
            this->isArray = isArray;
            this->line = line;
            this->sizeArray = sizeArray;
        }
        string id;
        bool isArray;
        int line;
        int sizeArray;
};

class Parameter{
    public:
        Parameter(Type type, Declarator * declarator, bool isArray, int line){
            this->type = type;
            this->declarator = declarator;
            this->line = line;
        }
        Type type;
        Declarator* declarator;
        bool isArray;
        int line;
        int evaluateSemantic();
};

class Expr{
    public:
        int line;
        virtual Type getType() = 0;
        virtual int evaluateSemantic() = 0;
};


class IntExpr : public Expr{
    public:
        IntExpr(int value, int line){
            this->value = value;
            this->line = line;
        }
        int evaluateSemantic();
        int value;
        Type getType();
};

class BoolExpr : public Expr{
    public:
        BoolExpr(bool value, int line){
            this->value = value;
            this->line = line;
        }
        int evaluateSemantic();
        bool value;
        Type getType();
};

class FloatExpr : public Expr{
    public:
        FloatExpr(float value, int line){
            this->value = value;
            this->line = line;
        }
        int evaluateSemantic();
        float value;
        Type getType();
};

class BinaryExpr : public Expr{
    public:
        BinaryExpr(Expr * expr1, Expr *expr2, int line){
            this->expr1 = expr1;
            this->expr2 = expr2;
            this->line = line;
        }
        int evaluateSemantic();
        Expr * expr1;
        Expr *expr2;
        int line;
};


class PostIncrementExpr: public Expr{
    public:
        PostIncrementExpr(Expr * expr, int line){
            this->expr = expr;
            this->line = line;
        }
        int evaluateSemantic();
        Expr * expr;
        int line;
        Type getType();
};

class PostDecrementExpr: public Expr{
    public:
        PostDecrementExpr(Expr * expr, int line){
            this->expr = expr;
            this->line = line;
        }
        int evaluateSemantic();
        Expr * expr;
        int line;
        Type getType();
};

class IdExpr : public Expr{
    public:
        IdExpr(string id, int line){
            this->id = id;
            this->line = line;
        }
        int evaluateSemantic();
        string id;
        int line;
        Type getType();
};

class ArrayExpr : public Expr{
    public:
        ArrayExpr(IdExpr * id, Expr * expr, int line){
            this->id = id;
            this->expr = expr;
            this->line = line;
        }
        int evaluateSemantic();
        IdExpr * id;
        Expr * expr;
        int line;
        Type getType();
};

class ArrayInitExpr: public Expr
{
   public:
        ArrayInitExpr(int size, int type,ArgumentList args , int line){
            this->size = size;
            this->type = type;
            this->args = args;
            this->line = line;
        }
        int evaluateSemantic();
        int size;
        int type;
        ArgumentList args;
        int line;
        Type getType();   
};

class MethodInvocationExpr : public Expr{
    public:
        MethodInvocationExpr(IdExpr * id, ArgumentList args, int line){
            this->id = id;
            this->args = args;
            this->line = line;
        }
        int evaluateSemantic();
        IdExpr * id;
        ArgumentList args;
        int line;
        Type getType();

};

class StringExpr : public Expr{
    public:
        StringExpr(string value, int line){
            this->value = value;
            this->line = line;
        }
        int evaluateSemantic();
        string value;
        int line;
        Type getType();
};


class StringValue{
    public:
        StringValue(string value, int line){
            this->value = value;
            this->line = line;
        }
        int evaluateSemantic();
        string value;
        int line;
};


class AssignExpr : public BinaryExpr{
    public: 
        AssignExpr(Expr * expr1, Expr *expr2,int type, int line) : BinaryExpr(expr1, expr2, line)
        {
            this->expr1 = expr1;
            this->expr2 = expr2;
            this->type = type;
            this->line = line ;
        }
        int evaluateSemantic();
        Expr * expr1;
        Expr *expr2;
        int type;
        int line;
        Type getType();
};

#define IMPLEMENT_BINARY_EXPR(name) \
class name##Expr : public BinaryExpr{\
    public: \
        name##Expr(Expr * expr1, Expr *expr2, int line) : BinaryExpr(expr1, expr2, line){}\
        Type getType(); \
        int evaluateSemantic(); \
};

IMPLEMENT_BINARY_EXPR(Add);
IMPLEMENT_BINARY_EXPR(Sub);
IMPLEMENT_BINARY_EXPR(Mul);
IMPLEMENT_BINARY_EXPR(Div);
IMPLEMENT_BINARY_EXPR(Eq);
IMPLEMENT_BINARY_EXPR(Neq);
IMPLEMENT_BINARY_EXPR(Gte);
IMPLEMENT_BINARY_EXPR(Lte);
IMPLEMENT_BINARY_EXPR(Gt);
IMPLEMENT_BINARY_EXPR(Lt);
IMPLEMENT_BINARY_EXPR(LogicalAnd);
IMPLEMENT_BINARY_EXPR(LogicalOr);
// IMPLEMENT_BINARY_EXPR(Assign);
IMPLEMENT_BINARY_EXPR(Percent);
IMPLEMENT_BINARY_EXPR(Pow);