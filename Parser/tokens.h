/* A Bison parser, made by GNU Bison 3.5.1.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2020 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

#ifndef YY_YY_TOKENS_H_INCLUDED
# define YY_YY_TOKENS_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 1 "tinyGo.y"

//     #include "ast.h"

#line 52 "tokens.h"

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    EOL = 258,
    TK_PRINTF = 259,
    TK_ADD = 260,
    TK_SUB = 261,
    TK_LESS = 262,
    TK_LEFT_BRACKET = 263,
    TK_RIGHT_BRACKET = 264,
    TK_MUL = 265,
    TK_POW = 266,
    TK_GREATER = 267,
    TK_DIV = 268,
    TK_EQUAL_EQUAL = 269,
    TK_COMMA = 270,
    TK_SEMICOLON = 271,
    TK_PERCENT = 272,
    TK_EXCLAMATION = 273,
    TK_COLON = 274,
    TK_LEFT_KEY = 275,
    TK_RIGHT_KEY = 276,
    TK_LEFT_PARENTHESES = 277,
    TK_RIGHT_PARENTHESES = 278,
    TK_LIT_FLOAT = 279,
    TK_LIT_INT = 280,
    TK_LIT_STRING = 281,
    TK_ID = 282,
    TK_INT = 283,
    TK_FLOAT = 284,
    TK_BOOL = 285,
    TK_STRING = 286,
    TK_MINUS_MINUS = 287,
    TK_PORCENT_OR_EQUAL = 288,
    TK_COLON_OR_EQUAL = 289,
    TK_PLUS_PLUS = 290,
    TK_DIV_OR_EQUAL = 291,
    TK_GREATER_OR_EQUAL = 292,
    TK_POT_OR_EQUAL = 293,
    TK_MUL_OR_EQUAL = 294,
    TK_LESS_OR_EQUAL = 295,
    TK_OR = 296,
    TK_OR_EQUAL = 297,
    TK_MINUS_EQUAL = 298,
    TK_NOT_EQUAL = 299,
    TK_EQUAL = 300,
    TK_AND = 301,
    TK_AND_EQUAL = 302,
    TK_PLUS_EQUAL = 303,
    TK_IF = 304,
    TK_ELSE = 305,
    TK_FALSE = 306,
    TK_TRUE = 307,
    TK_FOR = 308,
    TK_BREAK = 309,
    TK_CONTINUE = 310,
    TK_FUNC = 311,
    TK_RETURN = 312,
    TK_MAIN = 313,
    TK_IMPORT = 314,
    TK_PACKAGE = 315,
    TK_VAR = 316
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
union YYSTYPE
{
#line 22 "tinyGo.y"

    const char * string_t;
    int int_t;
    float float_t;

#line 131 "tokens.h"

};
typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_TOKENS_H_INCLUDED  */
