%option noyywrap
%option yylineno
%x comment

%{
    #include "tokens.h"
    #include <stdio.h>
%}

DIGIT [0-9]
ID [a-zA-Z]([a-zA-Z0-9])* 
STRINGVALUE [\"].+[\"] 
%%

[\n\r]+ { return EOL; }
[ \t]+ { /* ignorar */ }

"/*" {BEGIN(comment);}

"fmt.Println" { return TK_PRINTF;}
"break" {return TK_BREAK;}
"func" {return TK_FUNC;}
"else" {return TK_ELSE;}
"if" {return TK_IF;}
"continue" {return TK_CONTINUE;}
"for" { return TK_FOR;}
"import" {return TK_IMPORT;}
"return" {return TK_RETURN;}
"var" {return TK_VAR;}
"true" {return TK_TRUE;}
"false" { return TK_FALSE;}
"package" {return TK_PACKAGE;}
"string" { return TK_STRING;}
"int" {return TK_INT;}
"float32" {return TK_FLOAT;}
"bool" { return TK_BOOL;}
"main" { return TK_MAIN;}
{STRINGVALUE} { yylval.string_t = yytext; return TK_LIT_STRING; }
{DIGIT}+ { yylval.int_t = atoi(yytext); return TK_LIT_INT; }
{DIGIT}+"."{DIGIT}+ { yylval.float_t = atof(yytext); return TK_LIT_FLOAT; }
{ID} {yylval.string_t = yytext; return TK_ID; }

"+" {return TK_ADD; }
"+=" { return TK_PLUS_EQUAL; }
"&=" { return TK_AND_EQUAL; } //DUDA
"&&" { return TK_AND; }
"==" { return TK_EQUAL_EQUAL; }
"!=" { return TK_NOT_EQUAL; }
"-" {return TK_SUB; }
"-=" { return TK_MINUS_EQUAL; }
"|=" { return TK_OR_EQUAL; } //DUDA
"||" { return TK_OR; }
"<" { return TK_LESS; }
"<=" { return TK_LESS_OR_EQUAL; }
"[" { return TK_LEFT_BRACKET; }
"]" { return TK_RIGHT_BRACKET; }
"*" {return TK_MUL; }
"^" {return TK_POW; }
"*=" {return TK_MUL_OR_EQUAL; }
"^=" {return TK_POT_OR_EQUAL; }
">" { return TK_GREATER; }
">=" { return TK_GREATER_OR_EQUAL; }
"/" {return TK_DIV; }
"/=" { return TK_DIV_OR_EQUAL; }
"++" { return TK_PLUS_PLUS; }
"=" {return TK_EQUAL; }
":=" { return TK_COLON_OR_EQUAL; }
"," {return TK_COMMA; }
";" {return TK_SEMICOLON; }
"%" {return TK_PERCENT; }
"%=" { return TK_PORCENT_OR_EQUAL; }
"--" { return TK_MINUS_MINUS; }
"!" {return TK_EXCLAMATION; }
":" {return TK_COLON; }
"{" { return TK_LEFT_KEY; }
"}" { return TK_RIGHT_KEY; }
"(" { return TK_LEFT_PARENTHESES; }
")" { return TK_RIGHT_PARENTHESES; }

"//"[^\n]* {/* nada */}
. {printf("Caracter '%c' Invalido en la linea %d\n", yytext[0], (yylineno-1));}

<comment>"*/" {BEGIN(INITIAL);}
<comment>.|\n {/*nada*/}
<comment><<EOF>> {printf("Comentario incompleto en la linea %d\n", (yylineno-1)); return 0;}
%%