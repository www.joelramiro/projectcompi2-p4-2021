%code requires{
//     #include "ast.h"
}

%{
//http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1570.pdf
    #include <cstdio>
    using namespace std;
    int yylex();
    extern int yylineno;
    void yyerror(const char * s){
        fprintf(stderr, "Line: %d, error: %s\n", yylineno, s);
    }

    #define YYERROR_VERBOSE 1
    #define YYDEBUG 1
    #define EQUAL 1
    #define PLUSEQUAL 2
    #define MINUSEQUAL 3
%}

%union{
    const char * string_t;
    int int_t;
    float float_t;
}

%token EOL
%token TK_PRINTF
%token TK_ADD TK_SUB TK_LESS TK_LEFT_BRACKET TK_RIGHT_BRACKET TK_MUL TK_POW TK_GREATER TK_DIV TK_EQUAL_EQUAL TK_COMMA TK_SEMICOLON TK_PERCENT TK_EXCLAMATION TK_COLON TK_LEFT_KEY TK_RIGHT_KEY TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES
%token TK_LIT_FLOAT TK_LIT_INT TK_LIT_STRING TK_ID
%token TK_INT TK_FLOAT TK_BOOL TK_STRING
%token TK_MINUS_MINUS TK_PORCENT_OR_EQUAL TK_COLON_OR_EQUAL TK_PLUS_PLUS TK_DIV_OR_EQUAL TK_GREATER_OR_EQUAL TK_POT_OR_EQUAL TK_MUL_OR_EQUAL TK_LESS_OR_EQUAL TK_OR TK_OR_EQUAL TK_MINUS_EQUAL TK_NOT_EQUAL TK_EQUAL TK_AND TK_AND_EQUAL TK_PLUS_EQUAL
%token TK_IF TK_ELSE
%token TK_FALSE TK_TRUE
%token TK_FOR TK_BREAK TK_CONTINUE
%token TK_FUNC TK_RETURN
%token TK_MAIN TK_IMPORT TK_PACKAGE
%token TK_VAR

%%

    
start: package eol imports input
     | package eol input
    ;

package: TK_PACKAGE TK_MAIN
        ;

imports: imports import eol        
        | import eol
        | TK_IMPORT TK_LEFT_PARENTHESES eol importsLitString TK_RIGHT_PARENTHESES eol 
        ;

importsLitString:  TK_LIT_STRING eol importsLitString 
                  | TK_LIT_STRING eol
                ;

import: TK_IMPORT TK_LIT_STRING
        ;

input: input external_declaration
    | external_declaration
    ;

external_declaration: method_definition eol
            | declaration
            ;

method_definition: TK_FUNC TK_ID TK_LEFT_PARENTHESES parameters_type_list TK_RIGHT_PARENTHESES type block_statement
                 | TK_FUNC TK_ID TK_LEFT_PARENTHESES parameters_type_list TK_RIGHT_PARENTHESES TK_LEFT_BRACKET TK_LIT_INT TK_RIGHT_BRACKET type block_statement
                 | TK_FUNC TK_ID TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES type block_statement
                 | TK_FUNC TK_ID TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES TK_LEFT_BRACKET TK_LIT_INT TK_RIGHT_BRACKET type block_statement
                 | TK_FUNC TK_ID TK_LEFT_PARENTHESES parameters_type_list TK_RIGHT_PARENTHESES block_statement
                 | TK_FUNC TK_ID TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES block_statement
                 | TK_FUNC TK_MAIN TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES block_statement
                ;

declaration_list: declaration_list declaration
                | declaration
                ;

declaration: TK_VAR init_declarator eol
            ;

init_declarator: declarator type 
                | declarator type TK_EQUAL initializer
                ;
               
declarator: TK_ID  
          | TK_ID TK_LEFT_BRACKET TK_LIT_INT TK_RIGHT_BRACKET 
          ;

initializer: assignment_expression 
           | TK_LEFT_KEY initializer_list TK_RIGHT_KEY
           ;

parameters_type_list: parameters_type_list TK_COMMA parameter_declaration 
                   | parameter_declaration
                   ;
                   
parameter_declaration: declarator type
                    ;

initializer_list: initializer_list TK_COMMA logical_or_expression
                | logical_or_expression 
                ;

statement: expression_statement
        | if_statement eol
        | for_statement eol
        | block_statement eol
        | jump_statement
        | TK_PRINTF TK_LEFT_PARENTHESES expression_list TK_RIGHT_PARENTHESES eol
        ;


expression_list: expression_list TK_COMMA expression
               | expression
               ;

statement_list: statement_list statement
              | statement
              ;


block_statement: TK_LEFT_KEY eol statement_list TK_RIGHT_KEY
               | TK_LEFT_KEY eol declaration_list TK_RIGHT_KEY
               | TK_LEFT_KEY eol declaration_list statement_list TK_RIGHT_KEY
               | TK_LEFT_KEY eol statement_list declaration_list TK_RIGHT_KEY
               | TK_LEFT_KEY eol TK_RIGHT_KEY
               ;

if_statement: TK_IF  expression block_statement
            | TK_IF  expression block_statement TK_ELSE block_statement
            | TK_IF  expression block_statement TK_ELSE if_statement //♥
            | TK_IF  expression TK_SEMICOLON expression  block_statement
            | TK_IF  expression TK_SEMICOLON expression block_statement TK_ELSE block_statement
            | TK_IF  expression TK_SEMICOLON expression block_statement TK_ELSE if_statement //♥
            ;

for_statement: TK_FOR for_expression_statement for_expression_statement expression block_statement
             | TK_FOR expression block_statement
             | TK_FOR block_statement
            ;

for_expression_statement: expression TK_SEMICOLON
                        ;

expression_statement: expression eol
                    ;

jump_statement: TK_RETURN eol
              | TK_CONTINUE eol
              | TK_BREAK eol
              | TK_RETURN expression eol
              ;

type: TK_INT
    | TK_FLOAT
    | TK_BOOL
    | TK_STRING
    ;

primary_expression: TK_ID
    | constant
    |  TK_LEFT_PARENTHESES expression TK_RIGHT_PARENTHESES
    ;

 
assignment_expression: postfix_expression assignment_operator logical_or_expression
                     | logical_or_expression
                     ;

postfix_expression: primary_expression
                    | postfix_expression TK_LEFT_BRACKET expression TK_RIGHT_BRACKET
                    | postfix_expression TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES
                    | postfix_expression TK_LEFT_PARENTHESES argument_expression_list TK_RIGHT_PARENTHESES
                    | postfix_expression TK_PLUS_PLUS
                    | postfix_expression TK_MINUS_MINUS
                    | TK_LEFT_BRACKET TK_LIT_INT TK_RIGHT_BRACKET type TK_LEFT_KEY argument_expression_list TK_RIGHT_KEY
                    //asignar arreglos
                    ;


argument_expression_list: argument_expression_list TK_COMMA assignment_expression
                        | assignment_expression
                        ;
              
percent_expression: percent_expression TK_PERCENT postfix_expression
                  | postfix_expression
                  ;

pow_expression: pow_expression TK_POW percent_expression
              | percent_expression
              ;

multiplicative_expression: multiplicative_expression TK_MUL pow_expression
      | multiplicative_expression TK_DIV pow_expression
      | pow_expression
      ;
                  
additive_expression:  additive_expression TK_ADD multiplicative_expression
                    | additive_expression TK_SUB multiplicative_expression
                    | multiplicative_expression
                    ;

relational_expression: relational_expression TK_GREATER additive_expression
                     | relational_expression TK_LESS additive_expression
                     | relational_expression TK_GREATER_OR_EQUAL additive_expression
                     | relational_expression TK_LESS_OR_EQUAL additive_expression
                     | additive_expression
                     ;

equality_expression:  equality_expression TK_EQUAL_EQUAL relational_expression
                   | equality_expression TK_NOT_EQUAL relational_expression
                   | relational_expression
                   ;

logical_or_expression: logical_or_expression TK_OR logical_and_expression
                    //| logical_or_expression TK_OR_EQUAL logical_and_expression
                    | logical_and_expression
                    ;

logical_and_expression: logical_and_expression TK_AND equality_expression
                    // |  logical_and_expression TK_AND_EQUAL equality_expression
                      | equality_expression
                      ;

assignment_operator: TK_COLON_OR_EQUAL //declaramos variable y guardamos
                   | TK_EQUAL
                   | TK_PLUS_EQUAL
                   | TK_MINUS_EQUAL
                   | TK_POT_OR_EQUAL
                   | TK_DIV_OR_EQUAL
                   | TK_MUL_OR_EQUAL
                   | TK_PORCENT_OR_EQUAL
                   ;

expression: assignment_expression
          ;

constant: TK_LIT_INT
        | TK_LIT_FLOAT
        | TK_LIT_STRING
        | boolean
        ;

boolean: TK_TRUE
        | TK_FALSE
        ;
        
eol: EOL eol
    | EOL 
    ;
%%



