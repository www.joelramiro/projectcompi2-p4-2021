
start: package eol imports bodyProgram;

package: TK_PACKAGE TK_MAIN
        ;

bodyProgram: import bodyProgram     
           | function bodyProgram
           | main
           ;
           
imports: imports TK_IMPORT TK_LIT_STRING EOL
        | imports TK_IMPORT TK_LEFT_PARENTHESES importsLitString TK_RIGHT_PARENTHESES EOL         
        | imports EOL
        | 
        ;

importsLitString:  TK_LIT_STRING  EOL importsLitString
                  | TK_LIT_STRING
                  | eol importsLitString
                  | 
                ;


function: TK_FUNC TK_ID TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES TK_LEFT_KEY eol functionBody
        | TK_FUNC TK_ID TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES type TK_LEFT_KEY eol functionBody
        | TK_FUNC TK_ID TK_LEFT_PARENTHESES parameters TK_RIGHT_PARENTHESES TK_LEFT_KEY eol functionBody
        | TK_FUNC TK_ID TK_LEFT_PARENTHESES parameters TK_RIGHT_PARENTHESES type TK_LEFT_KEY eol functionBody
        ;
        

main: TK_FUNC TK_MAIN TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES TK_LEFT_KEY eol functionBody
        ;

functionBody: variable eol functionBody
        | assigment eol functionBody
        | funtionCall eol functionBody
        | TK_RIGHT_KEY eol
        ;

variable: TK_VAR TK_ID type 
        | TK_VAR TK_ID TK_LEFT_BRACKET TK_LIT_INT TK_RIGHT_BRACKET type
        ;

assigment: TK_ID TK_EQUAL literalValue
        | TK_ID TK_LEFT_BRACKET TK_LIT_INT TK_RIGHT_BRACKET TK_EQUAL literalValue 
        | TK_ID TK_EQUAL funtionCall
         ;

funtionCall: TK_ID TK_LEFT_PARENTHESES TK_RIGHT_PARENTHESES        
        | TK_ID TK_LEFT_PARENTHESES parameterList TK_RIGHT_PARENTHESES
        ;

parameterList: literalValue TK_COMMA parameterList
             | literalValue  
             ;   

parameters: parameters TK_COMMA parameter
        | parameter
        ;

parameter: TK_ID type
        | TK_ID TK_LEFT_BRACKET TK_LIT_INT TK_RIGHT_BRACKET type
        ;

type: TK_INT
     | TK_FLOAT
     | TK_BOOL 
     | TK_STRING
     ;

literalValue: TK_LIT_FLOAT
            | TK_LIT_STRING
            | TK_LIT_INT
            | TK_TRUE
            | TK_FALSE
            | TK_ID
            ;
