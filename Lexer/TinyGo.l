%option noyywrap
%option yylineno
%option caseless
%x comment

%{
    #include <stdio.h>
    #include "main.cpp"
    enum yytokentype{
        TK_BREAK = 258,
        TK_FUNC = 259,
        TK_ELSE = 260,
        TK_IF = 261,
        TK_CONTINUE = 262,
        TK_FOR = 263,
        TK_IMPORT = 264,
        TK_RETURN = 265,
        TK_VAR = 266,
        TK_TRUE = 267,
        TK_FALSE = 268,
        TK_PLUS_EQUAL = 269,
        TK_AND_EQUAL = 270,
        TK_AND = 271,
        TK_EQUAL = 272,
        TK_NOT_EQUAL = 273,
        TK_MINUS_EQUAL = 274,
        TK_OR_EQUAL = 275,
        TK_OR = 276,
        TK_LESS_OR_EQUAL = 277,
        TK_MUL_OR_EQUAL = 278,
        TK_POT_OR_EQUAL = 279,
        TK_GREATER_OR_EQUAL = 280,
        TK_DIV_OR_EQUAL = 281,
        TK_PLUS_PLUS = 282,
        TK_COLON_OR_EQUAL = 283,
        TK_PORCENT_OR_EQUAL = 284,
        TK_MINUS_MINUS = 285,
        TK_ID = 286,
        TK_LIT_INT = 287,
        TK_LIT_FLOAT = 288,
        TK_LIT_STRING = 289,
        TK_PACKAGE = 290,
        TK_INT = 291,
        TK_FLOAT = 292,
        TK_BOOL = 293,
        TK_STRING = 294,
        TK_MAIN = 295
    };
 
    int yylval;
%}

DIGIT [0-9]
ID [a-zA-Z]([a-zA-Z0-9])* 
STRINGVALUE [\"].+[\"] 
%%

[\t \r \n] { /* nada */ }
"/*" {BEGIN(comment);}

"break" {return TK_BREAK;}
"func" {return TK_FUNC;}
"else" {return TK_ELSE;}
"if" {return TK_IF;}
"continue" {return TK_CONTINUE;}
"for" { return TK_FOR;}
"import" {return TK_IMPORT;}
"return" {return TK_RETURN;}
"var" {return TK_VAR;}
"true" {return TK_TRUE;}
"false" { return TK_FALSE;}
"package" {return TK_PACKAGE;}
"string" { return TK_STRING;}
"int" {return TK_INT;}
"float32" {return TK_FLOAT;}
"bool" { return TK_BOOL;}
"main" { return TK_MAIN;}
")" { return ')'; }
{STRINGVALUE} { return TK_LIT_STRING; }
{DIGIT}+ { yylval = atoi(yytext); return TK_LIT_INT; }
{DIGIT}+"."{DIGIT}+ { return TK_LIT_FLOAT; }
{ID} {return TK_ID; }
"+" {return '+'; }
"+=" { return TK_PLUS_EQUAL; }
"&=" { return TK_AND_EQUAL; }
"&&" { return TK_AND; }
"==" { return TK_EQUAL; }
"!=" { return TK_NOT_EQUAL; }
"-" {return '-'; }
"-=" { return TK_MINUS_EQUAL; }
"|=" { return TK_OR_EQUAL; }
"||" { return TK_OR; }
"<" { return '<'; }
"<=" { return TK_LESS_OR_EQUAL; }
"[" { return '['; }
"]" { return ']'; }
"*" {return '*'; }
"^" {return '^'; }
"*=" {return TK_MUL_OR_EQUAL; }
"^=" {return TK_POT_OR_EQUAL; }
">" { return '>'; }
">=" { return TK_GREATER_OR_EQUAL; }
"/" {return '/'; }
"/=" { return TK_DIV_OR_EQUAL; }
"++" { return TK_PLUS_PLUS; }
"=" {return '='; }
":=" { return TK_COLON_OR_EQUAL; }
"," {return ','; }
";" {return ';'; }
"%" {return '%'; }
"%=" { return TK_PORCENT_OR_EQUAL; }
"--" { return TK_MINUS_MINUS; }
"!" {return '!'; }
":" {return ':'; }
"{" { return '{'; }
"}" { return '}'; }
"(" { return '('; }
")" { return ')'; }

"//"[^\n]* {/* nada */}
. {printf("Caracter '%c' Invalido en la linea %d\n", yytext[0], (yylineno-1));}

<comment>"*/" {BEGIN(INITIAL);}
<comment>.|\n {/*nada*/}
<comment><<EOF>> {printf("Comentario incompleto en la linea %d\n", (yylineno-1)); return 0;}
%%